<?php
  Class Quejob extends CI_Model
{
    
 function getfileinfo($fileid)   
 {
   $sql = 'SELECT * FROM files WHERE id = ? LIMIT 1';
   $query = $this->db->query($sql, array($fileid));  

   if($query -> num_rows()  > 0)
   {
        return $query->result_array();    
   }
   else
   {
       return false;
   }
    
 }
 function getvpsinfo($vps_ip, $vpsid)   
 {
   $sql = 'SELECT * FROM users WHERE id = ? AND vpsip = ? LIMIT 1';
   $query = $this->db->query($sql, array($vpsid, $vps_ip));  

   if($query -> num_rows()  > 0)
   {
        return $query->result_array();    
   }
   else
   {
       return false;
   }
    
 }
 function dovpslogin($iplogin,$vpsip)
 {
 $this->updateconnectedto($vpsip,$iplogin); 
 $this->addtologfile($iplogin,'['.date("F j, Y, g:i a",time()).']['.$vpsip.'] Logged into ['.$iplogin.']
 ');
 $this->addtologfile($vpsip,'['.date("F j, Y, g:i a",time()).']['.$vpsip.'] Logged into ['.$iplogin.']
'); 
redirect('connect/', 'refresh');  
 }
 function checkisslaved($slave_ip,$master_ip)
 {
     //grab current ip and pass for slave cause if changed before you login then it will need to be removed
      $slalvecurrentpass = $this->getcurrentpassword($slave_ip);
      $slavecurrentip =  $this->getcurrentip($slave_ip);
      //check is a slave then do a check to see if both passwords match return true else
      //if slave but not same password delete slave from list and return false if none above return false
      if ($this->checksalvedpassword($slavecurrentip,$master_ip,$slalvecurrentpass))
      {
         return true; 
      }else
      {
          if ($this->checksalvednomatch($slave_ip,$master_ip))
          {
                 $deadprocess = array(
            'vps_ip' => $slave_ip,
            'master_ip' => $master_ip
            );
     
        //delete the salve now
        $this->db->delete('slaves', $deadprocess);
        return false;
          }
          return false;
      }
      
 }
 function checksalvednomatch($ip,$masterip)
 {
    $query = $this->db->query('SELECT * FROM slaves WHERE vps_ip = ? AND master_ip = ? LIMIT 1',array($ip,$masterip));
   if($query->num_rows > 0)
   {
       return true;
   }else
   {
       return false;
   }   
 }
 function checksalvedpassword($ip,$masterip,$pass)
 {
    $query = $this->db->query('SELECT * FROM slaves WHERE vps_ip = ? AND master_ip = ? and password = ? LIMIT 1',array($ip,$masterip,$pass));
   if($query->num_rows > 0)
   {
       return true;
   }else
   {
       return false;
   }   
 }
 function getcurrentip($ip)
 {
   $query = $this->db->query('SELECT vpsip FROM users WHERE vpsip = ? LIMIT 1',array($ip));
   if($query->num_rows > 0)
   {
       $result = $query->result();
        foreach($result as $row)
     {
            return $row->vpsip;

         
     }
   }else
   {
       return false;
   }   
 }
 function getcurrentpassword($ip)
 {
   $query = $this->db->query('SELECT vpspass FROM users WHERE vpsip = ? LIMIT 1',array($ip));
   if($query->num_rows > 0)
   {
       $result = $query->result();
        foreach($result as $row)
     {
            return $row->vpspass;

         
     }
   }   
 }
 function getvpstpye($vpsip)
 {
   $query = $this->db->query('SELECT npc_type FROM users WHERE vpsip = ? LIMIT 1',array($vpsip));
   if($query->num_rows > 0)
   {
       $result = $query->result();
        foreach($result as $row)
     {
            return $row->npc_type;

         
     }
   }   
 }
 
 function crackvpspassword($vpsid,$ip,$timespent)
 {
       //here we want to make sure that when we crack the password if it failed or was good
       //and if so set slave to vpsid and record ips password to compare later if changed and needs recracking
       //also make sure user becomes conected to the vps so when it click the continue link it gets autologged in
       
       $basechance = 90;
       $crclevel = $this->getsoftlevel($vpsid,'.crc')*5;
       $totalchance = $basechance+$crclevel;
       $randchance = mt_rand(1,100);
       $this->updatetimespentcracking($vpsid,$timespent);
                    if ($randchance <= $totalchance) {
                        //succsess
                        //give rep based on time spent cracking to be done soon
                        $this->giverep($ip,($timespent/5));
                        $cracked = TRUE;
                    } else
                    {
                        //failed
                        //give rep based on time spent cracking to be done soon   
                        $this->giverep($ip,($timespent/5));; 
                        $cracked = FALSE;
                    }
                    
                    //if true set slave and set user connected other wise do nothing
                    if($cracked)
                    {
                      $vps_id = $this->getvpsid($ip);
                      $master_ip = $this->getvpsipbyid($vpsid);
                      $this->insertnewslave($master_ip,$ip,$this->getcurrentpassword($ip),$vpsid,$vps_id);
                      $this->updateconnectedto($master_ip,$ip); 
                      $this->addtologfile($ip,'['.date("F j, Y, g:i a",time()).']['.$master_ip.'] Logged into ['.$ip.']
');
                      $this->addtologfile($master_ip,'['.date("F j, Y, g:i a",time()).']['.$master_ip.']Cracked Password and Logged into ['.$ip.']
');  

                    }
                    
 }
 function updatetimespentcracking($id,$timespent)
 {
      $query = $this->db->query('UPDATE users SET time_spent_crack = time_spent_crack + ? WHERE id = ? LIMIT 1',array($timespent,$id));      
 }
 function giverep($ip,$rep)
 {
       $query = $this->db->query('UPDATE users SET reputation = ? WHERE vpsip = ? LIMIT 1',array($rep,$ip));     
 }
 /**
 * @function iscracking
 * 
 * @param mixed $vpsid
 * @param mixed $ip
 */
 function insertnewslave($master_ip,$ip,$password,$master_id,$id)
 {
     $newslave = array(
                'vps_ip' => $ip,
                'master_ip' => $master_ip,
                'password' => $password,
                'master_id' => $master_id,
                'vps_id' => $id
                
                );
        // instert status  + new array of data
        $this->db->insert('slaves', $newslave);
 }
 function updateconnectedto($vpsip,$conip)
 {
      $conupdate = array(
                'connected_to' => $conip
                );
        $this->db->where('vpsip', $vpsip);
        $this->db->update('users', $conupdate);
 }
 function iscracking($vpsid,$ip)
 {
   $query = $this->db->query('SELECT id, time_started, time_finished, type, status, vps_id, target_ip FROM process WHERE vps_id = ? and target_ip =  ? and status = "0"',array($vpsid,$ip));
   if($query->num_rows > 0)
   {
       return TRUE;
   }else
   {
       return FALSE;
   }
 }  
 /**
 * @function finish_active_process
 * 
 * this function will finish the processes for
 * certain vps
 * 
 * @param mixed $procid
 * @param mixed $status
 * @param mixed $vpsid
 */
 function finish_active_process($procid, $status = 0, $vpsid)
 {
   $query = $this->db->query('SELECT id, time_started, time_finished, type, status, vps_id, target_ip, filename, file_text, folder_id, file_id FROM process WHERE vps_id = '.$vpsid.' and id =  '.$procid.' and status ='.$status.' and time_finished<'.time().'');
   if($query->num_rows > 0)
   {
       $result = $query->result();
        foreach($result as $row)
     {
         switch($row->type){
         case 0:
            $this->changevpspassword($vpsid);
            $this->kill_process($row->id,$row->vps_id);
            
            redirect('mycomputer', 'refresh');
         break;
         case 1:   
            $this->changevpsip($vpsid);
            $this->kill_process($row->id,$row->vps_id);
            
            redirect('mycomputer', 'refresh'); 
         case 2: 
            $ip = $row->target_ip; 
            //work out time spent cracking
            $timespent_crack = $row->time_finished - $row->time_started; 
            $this->crackvpspassword($vpsid,$row->target_ip,$timespent_crack);
            $this->kill_process($row->id,$row->vps_id);
            
            redirect('internet/crack/'.$ip.'/1', 'refresh');
         break;
         case 3:
         $query2 = $this->db->query('SELECT temp_text FROM logfile WHERE vps_ip = ? LIMIT 1',array($row->target_ip));
         if($query2->num_rows > 0)
         {
         $result2 = $query2->result();
         foreach($result2 as $row2)
         {
            
         $hdfiles_array = array(
         'temp_text' => $row2->temp_text
         );
         
       
         $logfile=$hdfiles_array;
         $this->updatelogfile($row->target_ip,$logfile['temp_text']);
         

         }
         }
         $this->kill_process($row->id,$row->vps_id);
            
            redirect('logfile', 'refresh');   
         break;
         case 4:         
            $this->kill_process($row->id,$row->vps_id);
            $this->createnewfile($row->target_ip,$row->filename,$row->file_text,0,$row->folder_id);
            redirect('software/', 'refresh');
         break;
         case 5:            
            $this->kill_process($row->id,$row->vps_id);
            $this->createnewfolder($row->target_ip,$row->filename,'blank',0);
            redirect('software/', 'refresh');
         break;
         case 6:         
            $this->kill_process($row->id,$row->vps_id);
            $this->createnewfile($row->target_ip,$row->filename,$row->file_text,1,$row->folder_id);
            redirect('softwareext/', 'refresh');
         break;
         case 7:            
            $this->kill_process($row->id,$row->vps_id);
            $this->createnewfolder($row->target_ip,$row->filename,'blank',1);
            redirect('softwareext/', 'refresh');
         break;
         case 8:            
            $this->kill_process($row->id,$row->vps_id);
            $this->movefile($row->vps_id,$row->file_id,$row->folder_id);
            redirect('software/', 'refresh');
         break;
         case 9:            
            $this->kill_process($row->id,$row->vps_id);
            $this->movefile($row->vps_id,$row->file_id,$row->folder_id);
            redirect('softwareext/', 'refresh');
         break;
         case 10:            
            $this->kill_process($row->id,$row->vps_id);
            $this->hidefile($row->vps_id,$row->file_id);
            redirect('software/', 'refresh');
         break;
         case 11:            
            $this->kill_process($row->id,$row->vps_id);
            $this->hidefile($row->vps_id,$row->file_id);
            redirect('softwareext/', 'refresh');
         break;
         case 12:            
            $this->kill_process($row->id,$row->vps_id);
            $this->deletefile($row->vps_id,$row->file_id);
            redirect('software/', 'refresh');
         break;
         case 13:            
            $this->kill_process($row->id,$row->vps_id);
            $this->deletefile($row->vps_id,$row->file_id);
            redirect('softwareext/', 'refresh');
         break;
         case 14:            
            $this->kill_process($row->id,$row->vps_id);
            $this->uploadfile($row->vps_id,$row->file_id);
            redirect('softwareext/', 'refresh');
         break;
         case 15:            
            $this->kill_process($row->id,$row->vps_id);
            $this->downloadfile($row->vps_id,$row->file_id);
            redirect('software/', 'refresh');
         break;
         case 16:            
            $this->kill_process($row->id,$row->vps_id);
            $this->formathhd($row->vps_id,$row->target_ip,0);
            redirect('software/', 'refresh');
         break;
         case 17:            
            $this->kill_process($row->id,$row->vps_id);
            $this->formathhd($row->vps_id,$row->target_ip,1);
            redirect('softwareext/', 'refresh');
         break;
         case 18:            
            $this->kill_process($row->id,$row->vps_id);
            $this->editfile($row->vps_id,$row->target_ip,$row->file_text,$row->file_id,0);
            redirect('software/', 'refresh');
         break;
         case 19:            
            $this->kill_process($row->id,$row->vps_id);
            $this->editfile($row->vps_id,$row->target_ip,$row->file_text,$row->file_id,0); 
            redirect('softwareext/', 'refresh');
         break;
         case 20:            
            $this->kill_process($row->id,$row->vps_id);
            $this->installfile($row->vps_id,$row->file_id); 
            redirect('softwareext/', 'refresh');
         break;
         case 30://edit remote log file            
            $this->kill_process($row->id,$row->vps_id);
            $this->updatelogfile($row->target_ip,$row->file_text); 
            redirect('connect/', 'refresh');
         break;
         case 31:         
            $this->kill_process($row->id,$row->vps_id);
            $this->createnewfile($row->target_ip,$row->filename,$row->file_text,0,$row->folder_id);
            redirect('connect/files', 'refresh');
         break;
         case 32:            
            $this->kill_process($row->id,$row->vps_id);
            $this->createnewfolder($row->target_ip,$row->filename,'blank',0);
            redirect('connect/files', 'refresh');
         break;
         }
         
     }
   }
 }
 
 function editfile($vpsid,$target_ip,$text,$fileid,$hdd_remote)
{
     if ($hdd_remote != 0)
   {
       //its remote so lets get id and upload to remote hardrive
       $upload_id = $this->getvpsid($target_ip);
       $sql = '';
       $this->db->query($sql, array($vpsid,$hhd_type )); 
   }
   else
   {
     //its local use normal id in function and upload file 
       $time = time();
       $sql = 'UPDATE files SET text = ? , last_edit = ? WHERE id = ? AND vps_id = ?';
       $this->db->query($sql, array($text,$time,$fileid,$vpsid)); 
   }
   
}
 function formathhd($vpsid,$target_ip = 0,$hhd_type)
{
     if ($hhd_type != 0)
   {
       //its remote so lets get id and upload to remote hardrive
       $upload_id = $this->getvpsid($target_ip);
       $sql = 'DELETE FROM files WHERE vps_id = ? and hhd_type = 1';
       $this->db->query($sql, array($vpsid,$hhd_type )); 
   }
   else
   {
     //its local use normal id in function and upload file 
            $sql = 'DELETE FROM files WHERE vps_id = ? and hhd_type = 0';
       $this->db->query($sql, array($vpsid,$hhd_type )); 
   }
   
}

 function downloadfile($vpsid,$file_id)
{
   //get file information   
   $query = $this->db->query('SELECT * FROM files WHERE id = ? LIMIT 1',array($file_id));  

   if($query -> num_rows() == 1)
   {
      $row = $query->row();
     //its local drive and remote can share this function
     $sql = 'insert into files (vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl ,ext, text, hhd_type, is_folder,folder_id) 
        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
       $this->db->query($sql, array($vpsid, $row->type, $row->image, $row->filename, $row->level, $row->size, time(), 0,'0.0',$row->ext, $row->text,0,0,0)); 
   }
   else
   {
     return false;
   }
   
}
 
 function uploadfile($vpsid,$file_id,$target_ip = 0)
{
   //get file information
   $query = $this->db->query('SELECT * FROM files WHERE id = ? LIMIT 1',array($file_id));  

   if($query -> num_rows() == 1)
   {
      $row = $query->row();
       //lets see if remote or local
     if ($target_ip != 0)
   {
       //its remote so lets get id and upload to remote hardrive
       $upload_id = $this->getvpsid($target_ip);
       $sql = 'insert into files (vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl ,ext, text, hhd_type, is_folder,folder_id) 
        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
       $this->db->query($sql, array($upload_id, $row->type, $row->image, $row->filename, $row->level, $row->size, time(), 0,'0.0',$row->ext, $row->text,0,0,0)); 
   }
   else
   {
     //its local use normal id in function and upload file 
     $sql = 'insert into files (vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl ,ext, text, hhd_type, is_folder,folder_id) 
        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
       $this->db->query($sql, array($vpsid, $row->type, $row->image, $row->filename, $row->level, $row->size, time(), 0,'0.0',$row->ext, $row->text,1,0,0)); 
   }
   }
   else
   {
     return false;
   }
   
}
 function deletefile($vpsid,$file_id)
{
   $query = $this->db->query('DELETE FROM files WHERE vps_id = ? AND id = ? LIMIT 1',array($vpsid,$file_id));              
}
 function hidefile($vpsid,$file_id)
{
    $hide_lvl = $this->getsoftlevel($vpsid,'.hdr');
   $query = $this->db->query('UPDATE files SET hidden = ? , hidden_lvl = ? , last_edit = ? WHERE vps_id = ? AND id = ? LIMIT 1',array(1,$hide_lvl,time(),$vpsid,$file_id));              
}
 function movefile($vpsid,$file_id,$folder_id)
{
   $query = $this->db->query('UPDATE files SET folder_id = ? , last_edit = ? WHERE vps_id = ? AND id = ? LIMIT 1',array($folder_id,time(),$vpsid,$file_id));              
}
 function createnewfile($ip,$filename,$file_text,$type,$folder_id = 0)
 {
   $vpsid = $this->getvpsid($ip);
   $sql = 'insert into files (vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl ,ext, text, hhd_type, is_folder,folder_id) 
        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
   $this->db->query($sql, array($vpsid, 1, 'assets/layout/txt.jpg', $filename, 0, 1048576, time(), 0,'0.0','.txt', $file_text,$type,0,$folder_id)); 
 }
 function createnewfolder($ip,$filename= 'unnamed',$file_text = 'blank',$type,$folder_id = 0)
 {
   $vpsid = $this->getvpsid($ip);
   $sql = 'insert into files (vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl ,ext, text, hhd_type, is_folder,folder_id) 
        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
   $this->db->query($sql, array($vpsid, 0, 'assets/layout/folder.jpg', $filename, 0, 1048576, time(), 0,'0.0','/', $file_text,$type,1,$folder_id)); 
 }
 /**
 * @function get_active_process_by_type
 * will pull only processes by type.
 * 
 * 0 - change vps password
 * 1 -change vps ip
 * 
 * @param mixed $vpsid
 * @param mixed $type
 */
 function get_active_process_by_type($vpsid,$type)
 {
   $this -> db -> select('id, time_started, type, status, vps_id,time_finished');
   $this -> db -> from('process');
   $this -> db -> where('vps_id', $vpsid);
   $this -> db -> where('type', $type);
   

   $query = $this -> db -> get();

   if($query -> num_rows()  > 0)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 /**
 * @function get_active_process
 * will pull all processes.
 * 
 * @param mixed $vpsid
 */
 function get_active_process($vpsid)
 {
   $this -> db -> select('id, time_started, type, status, vps_id,time_finished');
   $this -> db -> from('process');
   $this -> db -> where('vps_id', $vpsid);
   

   $query = $this -> db -> get();

   if($query -> num_rows()  > 0)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 /**
 * @function instert_new_process
 * this function will insert any type of process you need
 * 
 * 0 - change vps password
 * 1 - change vps ip
 * 2 - crack vps password
 * 
 * @param mixed $vpsid
 * @param mixed $type
 */
 function instert_new_process($vpsid,$type,$timespent,$target_ip = '1.1.1.1',$filename = 'unnamed',$file_text='blank',$folder_id = 0,$fileid =0)
 {
     
     switch($type){
     case 0:
        //build new data array
        $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 0,
                'time_started' => time(),
                'time_finished' => time()+15,
                'status' => 0
                );
        // instert status  + new array of data
        $this->db->insert('process', $newprocess);
        //update users time spent
        $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
        $this->db->where('id', $vpsid);
        $this->db->update('users', $timeupdate); 
        break;
     case 1:
         // instert status
         //build new data array
         $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 1,
                'time_started' => time(),
                'time_finished' => time()+60, 
                'status' => 0
                );
         // instert status  + new array of data
         $this->db->insert('process', $newprocess);
         //update users time spent
        $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
        $this->db->where('id', $vpsid);
        $this->db->update('users', $timeupdate);        
        break;
     case 2:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 2,
                'time_started' => time(),
                'time_finished' => time()+12, 
                'status' => 0,
                'target_ip' => $target_ip
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break; 
     case 3:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 3,
                'time_started' => time(),
                'time_finished' => time()+20, 
                'status' => 0,
                'target_ip' => $target_ip
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;
     case 4:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 4,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;
     case 5:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 5,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;  
     case 6:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 6,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;
     case 7:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 7,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break; 
      case 8:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 8,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break; 
     case 9:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 9,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break; 
     case 10:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 10,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break; 
     case 11:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 11,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break; 
     case 12:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 12,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break; 
     case 13:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 13,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;
     case 14:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 14,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;  
      case 15:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 15,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;  
     case 16:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 16,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;  
     case 17:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 17,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;
      case 18:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 18,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;  
      case 19:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 19,
                'time_started' => time(),
                'time_finished' => time()+30, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;
     case 20:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 20,
                'time_started' => time(),
                'time_finished' => time()+15, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;
       case 30:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 30,
                'time_started' => time(),
                'time_finished' => time()+15, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break; 
       case 31:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 31,
                'time_started' => time(),
                'time_finished' => time()+15, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;        
      case 32:
     //build array
           $newprocess = array(
                'vps_id' => $vpsid,
                'type' => 32,
                'time_started' => time(),
                'time_finished' => time()+15, 
                'status' => 0,
                'target_ip' => $target_ip,
                'filename' => $filename,
                'file_text' => $file_text,
                'folder_id' => $folder_id,
                'file_id' => $fileid
                );
           $this->db->insert('process', $newprocess);      
           //update users time spent
           $timeupdate = array(
                'last_update' => time(),
                'time_spent' => $timespent
                );
           $this->db->where('id', $vpsid);
           $this->db->update('users', $timeupdate);
           break;               
     }
     
 }
 /**
 * @function kill_process
 * this will kill any vps process when called 
 * 
 * @param mixed $processid
 * @param mixed $vpsid
 */
 function kill_process($processid, $vpsid)
 {
     //ok lets kill process MMMM Memory Nom Nom Nom
     //build new data array
     $deadprocess = array(
            'id' => $processid,
            'vps_id' => $vpsid
            );
     
     //delete the process now
     $this->db->delete('process', $deadprocess);
     
 }
 function getsoftlevel($vpsid,$filetype)
 {
   $id = $vpsid;
   $query1 = $this->db->query('SELECT level FROM files WHERE vps_id = ? AND ext = ? ORDER BY level DESC LIMIT 1',array($id,$filetype));
   if($query1->num_rows > 0)
   {
        $row1 = $query1->row();
        return $row1->level;
   }else
   {
        return 0;  
   } 
 }
  function cancrack($vpsip,$vpsid)
 {
   $id = $this->getvpsidbyip($vpsip);
   $query1 = $this->db->query('SELECT level FROM files WHERE vps_id = ? AND ext = ".fwl" ORDER BY level DESC LIMIT 1',array($id));
   if($query1->num_rows > 0)
   {
       $row1 = $query1->row();
       $targetfiles = array( 
         'level'=>$row1->level, 
       );
       
       $target=$targetfiles;
   }else
   {
     $targetfiles = array( 
         'level'=>0, 
       );
       
       $target=$targetfiles;  
   } 
   $query2 = $this->db->query('SELECT level FROM files WHERE vps_id = ? AND ext = ".wwl" ORDER BY level DESC LIMIT 1',array($vpsid));
   if($query2->num_rows > 0)
   {
       $row2 = $query2->row();
       $playerfiles = array( 
         'level'=>$row2->level, 
       );
       
       $player=$playerfiles;
   }else
   {
    $playerfiles = array( 
         'level'=>0, 
       );
       
       $player=$playerfiles;
   }
    
   if($player['level'] >= $target['level'])
   {
        
       return true;
   }
   else
   {
        return false;
   }
 }
 function getvpsipbyip($ip)
 {
  $query = $this->db->query('SELECT vpsip FROM users WHERE vpsip = ? LIMIT 1',array($ip));  

   if($query -> num_rows() == 1)
   {
     $row = $query->row();
     return $row->vpsip;
   }
   else
   {
     return false;
   }
 }
 function getvpsipbyid($id)
 {
  $query = $this->db->query('SELECT vpsip FROM users WHERE id = ? LIMIT 1',array($id));  

   if($query -> num_rows() == 1)
   {
     $row = $query->row();
     return $row->vpsip;
   }
   else
   {
     return false;
   }
 }
 function getvpsid($ip)
 {
  $query = $this->db->query('SELECT id FROM users WHERE vpsip = ? LIMIT 1',array($ip));  

   if($query -> num_rows() == 1)
   {
     $row = $query->row();
     return $row->id;
   }
   else
   {
     return false;
   }
 }
 function getvpsidbyip($id)
 {
  $query = $this->db->query('SELECT id FROM users WHERE id = ? LIMIT 1',array($id));  

   if($query -> num_rows() == 1)
   {
     $row = $query->row();
     return $row->id;
   }
   else
   {
     return false;
   }
 }
 function webinfo($ip)
 {
   
   $sql = "SELECT vpsip,text FROM webservice WHERE vpsip = ? LIMIT 1"; 

   $query = $this->db->query($sql, array($ip));  

   if($query -> num_rows() == 1)
   {
      $result = $query->result();
        foreach($result as $row)
     {
            return $row->text;
     }
   }
   else
   {
       return '<table style="margin-left: auto;margin-right: auto;">
            <tbody>
                <tr>
                    <td>404 No such webservice.
                    </td>
                </tr>
                </tbody>
        </table>';
   }
     
 }
 function conecctedtostatus($ip)
 {
   $sql = "SELECT connected_to FROM users WHERE vpsip = ? LIMIT 1"; 

   $query = $this->db->query($sql, array($ip));  

   if($query -> num_rows() == 1)
   {
     $row = $query->row();  
     if($row->connected_to != 0)
     {
         return true;
     }
     else
     {
         return false;
     }
   }
 }
 function conecctedtoinfo($ip)
 {
   $sql = "SELECT connected_to FROM users WHERE vpsip = ? LIMIT 1"; 

   $query = $this->db->query($sql, array($ip));  

   if($query -> num_rows() == 1)
   {
     $row = $query->row();  
     if($row->connected_to)
     {
         return 'you are currently conected to '.anchor('connect',  $row->connected_to, array('title' => 'Access vps!'));
     }
     else
     {
         return 'Not connected to any vps at this time';
     }
   }
 }
 function pinginfo($ip,$vpsip)
 {
   
   $sql = "SELECT vpsip FROM users WHERE vpsip = ? LIMIT 1"; 

   $query = $this->db->query($sql, array($ip));  

   if($query -> num_rows() == 1)
   {
       if($ip == $vpsip)
       { 
            return '<div style="background-color: #3498db;">You were able to ping the server.
            </div>';
        }
        else
        {
            return '<div style="background-color: #27ae60;">You were able to ping the server.
            <br>'.anchor('internet/login/'.$ip.'',  'Go to login screen', array('title' => 'Leet cracks ahead!')).'
            </div>';
        }
   }
   else
   {
       return '<div style="background-color: #e74c3c;">You were not able to ping the server.
        </div>';
   }
     
 }
 function getcurrentlogtext($ip)
 {
     $query = $this->db->query('SELECT text FROM logfile WHERE vps_ip = ? LIMIT 1',array($ip));  

   if($query -> num_rows() == 1)
   {
     $row = $query->row();
     return $row->text;
   }
   else
   {
     return false;
   }
 }
 function addtologfile($ip,$text){
        $text1 = $this->getcurrentlogtext($ip);
        $newtext = $text.$text1;
        $query = $this->db->query('UPDATE logfile SET text = ? WHERE vps_ip = ? LIMIT 1',array($newtext,$ip)); 
 }
 function updatelogfile($ip,$text){
        $query = $this->db->query('UPDATE logfile SET text = ? WHERE vps_ip = ? LIMIT 1',array($text,$ip)); 
 }
 
 function updatelogfiletemp($ip,$text){
        $query = $this->db->query('UPDATE logfile SET temp_text = ? WHERE vps_ip = ? LIMIT 1',array($text,$ip)); 
 }
 /**
 * @function changevpspassword
 * this function will change the selected vps password 
 * 
 * @param mixed $vpsid
 */
 function changevpspassword($vpsid)
 {
     $vpspass='';
     for ($i=0;$i<7;$i++){
        if (rand(0,1)){
            $vpspass.=chr( rand(ord('A'),ord('Z')) );
        }else{
            $vpspass.=chr( rand(ord('0'),ord('9')) );
        }
    }
       $data = array(
           'vpspass' => $vpspass,
           'last_update' => time()
        );

        $this->db->where('id', $vpsid);
        $this->db->update('users', $data); 
 }
 /**
 * @function changevpsip
 * this function will change ip address of vps
 * 
 * TODO
 * -remove off enemy slave list if not infected with a virus
 *  will most like add another function and call it after this one
 * 
 * @param mixed $vpsid
 */
 function changevpsip($vpsid)
 {
       $vpsip=rand(15,255).".".rand(0,255).".".rand(0,255).".".rand(0,255);
       $query = $this->db->query('SELECT ip_resets FROM users WHERE id = ? LIMIT 1',array($vpsid)); 
       $ipreset = 0;
       if($query -> num_rows() == 1)
       {
         foreach ($query->result() as $row)
         {
            $ipreset = $row->ip_resets + 1;
         }   
       }     
     
       $data = array(
           'ip_resets' => $ipreset,
           'vpsip' => $vpsip,
           'last_update' => time(),
           'last_ip_reset' => time()
        );

        $this->db->where('id', $vpsid);
        $this->db->update('users', $data);
        $query = $this->db->query('SELECT vpsip FROM users WHERE id = ? LIMIT 1',array($vpsid)); 
       
       if($query -> num_rows() == 1)
       {
         foreach ($query->result() as $row)
         {
            $newip = $row->vpsip;
         }   
       } 
        $query = $this->db->query('UPDATE slaves SET master_ip = ? WHERE master_id = ?',array($newip,$vpsid)); 
        $query = $this->db->query('UPDATE logfile SET vps_ip = ? WHERE vps_id = ?',array($newip,$vpsid)); 
       
            
 }
 function installfile($vpsid,$fileid)
 {
     $filetype = $this->getfileinfo($fileid);
     $newtype = "";
     $newimage = "";
    // var_dump($filetype);die();exit();
     switch($filetype[0]["type"]){
     case "10":
     $newtype = 27;
     $newimage = "assets/layout/activevspam.jpg";
     break;
     case "11":
     $newtype = 27;
     $newimage = "assets/layout/activevspam.jpg";
     break;
     case "12":
     $newtype = 27;
     $newimage = "assets/layout/activevspam.jpg";
     break;
     case "21":
     $newtype = 27;
     $newimage = "assets/layout/activevspam.jpg";
     break;
     case "23":
     $newtype = 27;
     $newimage = "assets/layout/activevspam.jpg";
     break;    
     }
     $this->db->query('UPDATE files SET master_id = ? , active = "1" , type = ? , image= ?   WHERE id = ?',array($vpsid,$newtype,$newimage,$fileid));                  
 }
 /**
 * @function getactiveprocesslist
 * this will build the progreess bars and options for each process
 * and cshould be used in the process page
 * 
 * @param mixed $vpsid
 */
 function getactiveprocesslist($vpsid)
 {
   $this -> db -> select('id, time_started, type, status, vps_id,time_finished');
   $this -> db -> from('process');
   $this -> db -> where('vps_id', $vpsid);
   

   $query = $this -> db -> get();

   if($query -> num_rows()  > 0)
   {
     $result = $query->result();
     $returnstring ='';
     $hdfiles_array = array();
     foreach($result as $row)
     {
         
       $hdfiles_array = array(
         'id' => $row->id,
         'type' => $row->type,//TPYE_.$row->type,
         'time_started' => $row->time_started,
         'time_finished' => $row->time_finished
         
       );
       
       $process=$hdfiles_array;
       $mpc_config = $this->config->item('slavehack');  
       $returnstring .= "
       <script type='text/javascript'>
       var pressed".$process['id']." = false; 
       var finished".$process['id']." = false; 

       var date = new Date();
       var milisecStarted".$process['id']."=date.getTime()-0;
       var milisecEnd".$process['id']."=date.getTime()+".($process['time_finished']-time())."*1000;
       var milisecTotalTime".$process['id']."=milisecEnd".$process['id']."-milisecStarted".$process['id'].";

       function prog".$process['id']."()
       {
            if (pressed".$process['id']." == false) 
            { 
            pressed".$process['id']." = true; 
            progBar".$process['id']."(); 
            }
       } 
       function progBar".$process['id']."()
       {
            if (finished".$process['id']." == false )
            {
                var now = new Date();
                milisecNow".$process['id']."=now.getTime();
        
                var miliSecondsLeft".$process['id']."=Math.ceil((milisecEnd".$process['id']."-milisecNow".$process['id'].")/100);
                if(miliSecondsLeft".$process['id']."<0){miliSecondsLeft".$process['id']."=0;}
                                                                     
                var hours".$process['id']."=Math.floor(miliSecondsLeft".$process['id']."/36000);
                var minutes".$process['id']."=Math.floor(miliSecondsLeft".$process['id']."%36000/600);
                var seconds".$process['id']."=Math.floor(miliSecondsLeft".$process['id']."%36000%600)/10;
                if((seconds".$process['id']."%1)==0)
                {
                    seconds".$process['id']."=seconds".$process['id']."+\".0\";
                }
                var output".$process['id'].";
                if(hours".$process['id'].">0)
                {
                    var output".$process['id']."=hours".$process['id']."+\" hours, \"+minutes".$process['id']."+\" minutes and \"+seconds".$process['id']."+\" seconds.\";
                }
                else
                {
                    if(minutes".$process['id'].">0)
                    {
                        var output".$process['id']."=minutes".$process['id']."+\" minute(s) and \"+seconds".$process['id']."+\" seconds.\";
                    }
                    else
                    {
                        var output".$process['id']."=seconds".$process['id']."+\" seconds.\";
                    }
                }        
        
                miliSecondsLeft".$process['id']."=Math.floor(miliSecondsLeft".$process['id'].")/10;        
                var procent".$process['id']."=Math.floor(((milisecTotalTime".$process['id']."-miliSecondsLeft".$process['id']."*1000)/milisecTotalTime".$process['id'].")*100);
                if(procent".$process['id']."<=0)
                { 
                    procent".$process['id']."=1; 
                }
                if (milisecEnd".$process['id'].">=milisecNow".$process['id'].")
                {
                    document.getElementById(\"d3".$process['id']."\").innerHTML= output".$process['id'].";
                    document.getElementById(\"d1".$process['id']."\").innerHTML=procent".$process['id']."+\"%\";            
            
                } 
                else 
                {                                                                           
                    finished".$process['id']." = true;            
            
                    document.getElementById(\"d1".$process['id']."\").innerHTML=\"100% ready\";            
                    document.getElementById(\"d3".$process['id']."\").innerHTML=\"<input class=form type=submit value=Continue></form>\";        
                }        
                document.getElementById(\"d2".$process['id']."\").style.width=(procent".$process['id']."/100)*300+\"px\";           
                setTimeout(\"progBar".$process['id']."();\", 100);    
            }
        }
        window.setTimeout(\"prog".$process['id']."()\",151);

        </script>
        <tr bgcolor=#222222 valign=top>
        <td>
        <small>
        <a href='".base_url("processes/kill/".$process['id']."")."'>Kill process</a>  
        </small>
        <td>
        <center>
        <form name=\"WorkForm".$process['id']."\" action=\"".base_url("processes/finishtask/".$process['id']."")."\" method=post>
        <input type=hidden value=workscreen value=1>
        <br />
        <br />
        <small>".$mpc_config['process'][$process['type']]."</small>
        <table border=1 style='margin-left: auto; margin-right: auto;'>
        <tr>
        <td>
        <div id=\"empty\" style=\"background-color:#222222;border:1px solid black;width:300px;padding:0px;padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:0px;\" align=\"left\">
        <div id=\"d2".$process['id']."\" style=\"position:relative;top:0px;left:0px;background-color:#666666;width:0px;padding-top:5px;padding:0px;\">
        <div id=\"d1".$process['id']."\" style=\"position:relative;top:0px;left:0px;color:#f0ffff;padding:0px;padding-top:0px;\"></div>
        </div>
        </div>
        </td>
        </tr>
        </table>
        <div id=\"d3".$process['id']."\" style=\"position:relative;top:0px;left:0px;height:30px;text-align:center;padding:0px;padding-top:5px;\">
        </div>
        </form>
        </td>
        </tr>
        <tr height=2><td></td><td><img src=".base_url("assets/layout/pixel.gif")."> 
        </td>
        ";
     }
     return $returnstring;
   }
 }
 
 function getactiveprocesslistbytype($vpsid,$type)
 {
   $this -> db -> select('id, time_started, type, status, vps_id,time_finished');
   $this -> db -> from('process');
   $this -> db -> where('vps_id', $vpsid);
   $this -> db -> where('type', $type); 
   

   $query = $this -> db -> get();

   if($query -> num_rows()  > 0)
   {
     $result = $query->result();
     $returnstring =false;
     $hdfiles_array = array();
     foreach($result as $row)
     {
         
       $hdfiles_array = array(
         'id' => $row->id,
         'type' => $row->type,//TPYE_.$row->type,
         'time_started' => $row->time_started,
         'time_finished' => $row->time_finished
         
       );
       
       $process=$hdfiles_array;
       $mpc_config = $this->config->item('slavehack');  
       $returnstring .= "
       <script type='text/javascript'>
       var pressed".$process['id']." = false; 
       var finished".$process['id']." = false; 

       var date = new Date();
       var milisecStarted".$process['id']."=date.getTime()-0;
       var milisecEnd".$process['id']."=date.getTime()+".($process['time_finished']-time())."*1000;
       var milisecTotalTime".$process['id']."=milisecEnd".$process['id']."-milisecStarted".$process['id'].";

       function prog".$process['id']."()
       {
            if (pressed".$process['id']." == false) 
            { 
            pressed".$process['id']." = true; 
            progBar".$process['id']."(); 
            }
       } 
       function progBar".$process['id']."()
       {
            if (finished".$process['id']." == false )
            {
                var now = new Date();
                milisecNow".$process['id']."=now.getTime();
        
                var miliSecondsLeft".$process['id']."=Math.ceil((milisecEnd".$process['id']."-milisecNow".$process['id'].")/100);
                if(miliSecondsLeft".$process['id']."<0){miliSecondsLeft".$process['id']."=0;}
                                                                     
                var hours".$process['id']."=Math.floor(miliSecondsLeft".$process['id']."/36000);
                var minutes".$process['id']."=Math.floor(miliSecondsLeft".$process['id']."%36000/600);
                var seconds".$process['id']."=Math.floor(miliSecondsLeft".$process['id']."%36000%600)/10;
                if((seconds".$process['id']."%1)==0)
                {
                    seconds".$process['id']."=seconds".$process['id']."+\".0\";
                }
                var output".$process['id'].";
                if(hours".$process['id'].">0)
                {
                    var output".$process['id']."=hours".$process['id']."+\" hours, \"+minutes".$process['id']."+\" minutes and \"+seconds".$process['id']."+\" seconds.\";
                }
                else
                {
                    if(minutes".$process['id'].">0)
                    {
                        var output".$process['id']."=minutes".$process['id']."+\" minute(s) and \"+seconds".$process['id']."+\" seconds.\";
                    }
                    else
                    {
                        var output".$process['id']."=seconds".$process['id']."+\" seconds.\";
                    }
                }        
        
                miliSecondsLeft".$process['id']."=Math.floor(miliSecondsLeft".$process['id'].")/10;        
                var procent".$process['id']."=Math.floor(((milisecTotalTime".$process['id']."-miliSecondsLeft".$process['id']."*1000)/milisecTotalTime".$process['id'].")*100);
                if(procent".$process['id']."<=0)
                { 
                    procent".$process['id']."=1; 
                }
                if (milisecEnd".$process['id'].">=milisecNow".$process['id'].")
                {
                    document.getElementById(\"d3".$process['id']."\").innerHTML= output".$process['id'].";
                    document.getElementById(\"d1".$process['id']."\").innerHTML=procent".$process['id']."+\"%\";            
            
                } 
                else 
                {                                                                           
                    finished".$process['id']." = true;            
            
                    document.getElementById(\"d1".$process['id']."\").innerHTML=\"100% ready\";            
                    document.getElementById(\"d3".$process['id']."\").innerHTML=\"<input class=form type=submit value=Continue></form>\";        
                }        
                document.getElementById(\"d2".$process['id']."\").style.width=(procent".$process['id']."/100)*300+\"px\";           
                setTimeout(\"progBar".$process['id']."();\", 100);    
            }
        }
        window.setTimeout(\"prog".$process['id']."()\",151);

        </script>
        <tr bgcolor=#222222 valign=top>
        <td>
        <small>
        <a href='".base_url("processes/kill/".$process['id']."")."'>Kill process</a>
        </small>
        <td>
        <center>
        <form name=\"WorkForm".$process['id']."\" action=\"".base_url("processes/finishtask/".$process['id']."")."\" method=post>
        <input type=hidden value=workscreen value=1>
        <br />
        <br />
        <small>".$mpc_config['process'][$process['type']]."</small>
        <table border=1 style='margin-left: auto; margin-right: auto;'>
        <tr>
        <td>
        <div id=\"empty\" style=\"background-color:#222222;border:1px solid black;width:300px;padding:0px;padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:0px;\" align=\"left\">
        <div id=\"d2".$process['id']."\" style=\"position:relative;top:0px;left:0px;background-color:#666666;width:0px;padding-top:5px;padding:0px;\">
        <div id=\"d1".$process['id']."\" style=\"position:relative;top:0px;left:0px;color:#f0ffff;padding:0px;padding-top:0px;\"></div>
        </div>
        </div>
        </td>
        </tr>
        </table>
        <div id=\"d3".$process['id']."\" style=\"position:relative;top:0px;left:0px;height:30px;text-align:center;padding:0px;padding-top:5px;\">
        </div>
        </form>
        </td>
        </tr>
        <tr height=2><td></td><td><img src=".base_url("assets/layout/pixel.gif").">
        </td>
        ";
     }
     return $returnstring;
   }
 }
 
 function getslaveslist($vpsip)
 {
   $query = $this->db->query('SELECT * FROM slaves WHERE master_ip = ?',array($vpsip));

   if($query -> num_rows()  > 0)
   {
     $result = $query->result();
     $returnstring =false;
     $slave_array = array();
     foreach($result as $row)
     {
         
       $slave_array = array(
         'id' => $row->id,
         'vps_ip' => $row->vps_ip,//TPYE_.$row->type,
         'master_ip' => $row->master_ip,
         'password' => $row->password,
         'task_type' => $row->task_type,
         'time_start_task' => $row->time_start_task,
         'vps_id' => $row->vps_id,
         'master_id'=> $row->master_id
         
       );
       
       $slave=$slave_array;
       $vps_array=$this->getvpsinfo($slave['vps_ip'],$slave['vps_id']);   
       $vps_info = $vps_array[0];
       $mpc_config = $this->config->item('slavehack');  
       $returnstring .= " <tr bgcolor=\"#222222\">
            <td><a href=".base_url("internet/?ip=".$slave['vps_ip']).">".$slave['vps_ip']."</a>
            </td>";
            if ($vps_info['vpspass'] == $slave['password']){
            $returnstring .= "<td><b>".$slave['password']."</b>";
            }
            else
            {
             $returnstring .= "<td><b>Password has changed</b>";     
            }
            $returnstring .= "</td>
            <td>";
            
            $returnstring .= "<b><a href=".base_url("slaves/work/".$slave['vps_ip']).">Spam virus</a> 0.7</b>";
            //$returnstring .= "<b><a href=\"index2.php?page=slaves&amp;slave=5168179&amp;work=vspam\">Spam virus</a> 0.7</b>";
            
            $returnstring .= "
            </td>
            <td><i>No DDoS virus</i>
            </td>
            <td><i>No sharewarez virus</i>
            </td>
            <td><i><small>No task assigned</small></i>
            </td>
            <td></td>
            <td><a href=\"?page=slaves&amp;DELETE=5168179\"><small>delete</small></a>
            </td>
        </tr>  ";
     }
     return $returnstring;
   }else 
   {
       $returnstring = '<tr bgcolor=\"#222222\"><td>cannot find any slaves';
       return $returnstring;    
   }
 }
 
 function dobotcheck($id,$hash)
 {
     
     $botcheckhtml ='
     
     <script>
 $(function() {
    Recaptcha.create("6LelceESAAAAAPub9vjNUZ0p1Ycjk_WIQiRV8fPV", "recaptcha_div", {
    theme: "white",
    callback: Recaptcha.focus_response_field});  
    
    function validateCaptcha()
{
    challengeField = $("input#recaptcha_challenge_field").val();
    responseField = $("input#recaptcha_response_field").val();
    vpsid = $("input#vpsid").val(); 
    hash = $("input#hash").val(); 
    var html = $.ajax({
    type: "POST",
    url: "ajax/recaptcha/",
    data: "recaptcha_challenge_field=" + challengeField + "&recaptcha_response_field=" + responseField + "&vpsid=" + vpsid+ "&hash=" + hash,
    async: false
    }).responseText;
    if(html == "success")
    {
        $("#captchaStatus").html(" ");
        return true;
    }
    else
    {
        $("#captchaStatus").html("You have not entered the right information");
        Recaptcha.reload();
        return false;
    }
}
 
    $( "#dialog-form" ).dialog({
      autoOpen: true,
      height: 280,
      width: 350,
      modal: true,
      buttons: {
        "Im not a BOT": function() {
          var bValid = true;
          
          bValid = bValid && validateCaptcha();
         
          if ( bValid ) {
            $( this ).dialog( "close" );
          }
        }
      }
    });
  });
  </script>
  
  <div id="dialog-form" title="Bot Check"> 
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<style>

  body { font-size: 62.5%; }
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  .ui-dialog-titlebar-close {
  visibility: hidden;
} .ui-widget-overlay {
    background: #000000 50% 50% repeat-x;
    opacity: 1;
    filter: Alpha(Opacity=100);
}
.ui-widget-shadow {
    margin: -8px 0 0 -8px;
    padding: 8px;
    background: grey 50% 50% repeat-x;
    opacity: .3;
    filter: Alpha(Opacity=30);
    border-radius: 8px;
}
  </style>
  <input type="hidden" name=vpsid id=vpsid value='.$id.'>
    <input type="hidden" name=hash id=hash value='.$hash.'> 
 <div id="recaptcha_div"></div>
 <p style="color: red;" id="captchaStatus">&nbsp;</p> 
</div>
  ';
     return $botcheckhtml;
 }
}
?>
