<?php

/**
 * @class user model
 *
 * This class with contain mojority of user functions
 *
 */
Class User extends CI_Model
{

    /**
     * @function random_tip
     *
     * This will pull a random tip from db
     *
     */
    function random_tip()
    {
        $this->db->select('id, tip');
        $this->db->from('tips');
        $this->db->order_by("id", "random");
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                $tip = $row->tip;
            }
            return $tip;
        } else {
            return false;
        }

    }

    /**
     * @function TimeLeft
     *
     * this function formats time left in a nice way
     *
     * @param mixed $timecount
     *
     * @return string
     */
    function TimeLeft($timecount)
    {
        $tc_year        = 365 * 24 * 60 * 60;
        $tc_day         = 24 * 60 * 60;
        $tc_hour        = 60 * 60;
        $tc_minutes     = 60;
        $TotalCountEcho = "";
        $TotalCountEcho = number_format($timecount) . "s";
        if ($timecount > $tc_minutes) {
            $tc_m           = floor($timecount / $tc_minutes);
            $TotalCountEcho = number_format($tc_m) . " min";
        }
        if ($timecount > $tc_hour) {
            $tc_h           = floor($timecount / $tc_hour);
            $TotalCountEcho = number_format($tc_h) . " hrs";
        }
        return $TotalCountEcho;
    }

    /**
     * @function TimeCount
     * this function formats time in a nice way
     * mainly used to tell when last seen
     * or last login ect ect
     *
     * @param mixed $timecount
     *
     * @return string
     */
    function TimeCount($timecount)
    {
        $tc_year        = 365 * 24 * 60 * 60;
        $tc_day         = 24 * 60 * 60;
        $tc_hour        = 60 * 60;
        $tc_minutes     = 60;
        $TotalCountEcho = "";
        if ($timecount > $tc_year) {
            $tc_y           = floor($timecount / $tc_year);
            $timecount      = $timecount - ($tc_year * $tc_y);
            $TotalCountEcho = number_format($tc_y) . " Years Ago";
        } elseif ($timecount > $tc_day) {
            $tc_d           = floor($timecount / $tc_day);
            $timecount      = $timecount - ($tc_day * $tc_d);
            $TotalCountEcho = number_format($tc_d) . " Days Ago";
        } elseif ($timecount > $tc_hour) {
            $tc_h           = floor($timecount / $tc_hour);
            $timecount      = $timecount - ($tc_hour * $tc_h);
            $TotalCountEcho = number_format($tc_h) . " Hours Ago";
        } elseif ($timecount > $tc_minutes) {
            $tc_m           = floor($timecount / $tc_minutes);
            $timecount      = $timecount - ($tc_minutes * $tc_m);
            $TotalCountEcho = number_format($tc_m) . " Mins Ago";
        } else {
            $TotalCountEcho = "Just Now";
        }
        return $TotalCountEcho;
    }

    /**
     * @function DisplayTotalTime
     * This function formats time in a nice way
     * Used mainly for time spent
     * eg: last_updated_time - current time = timecount
     *
     * @param mixed $timecount
     *
     * @return string
     */
    function DisplayTotalTime($timecount)
    {
        $tc_year        = 365 * 24 * 60 * 60;
        $tc_day         = 24 * 60 * 60;
        $tc_hour        = 60 * 60;
        $tc_minutes     = 60;
        $TotalCountEcho = "";
        if ($timecount > $tc_year) {
            $tc_y      = floor($timecount / $tc_year);
            $timecount = $timecount - ($tc_year * $tc_y);
            $TotalCountEcho .= number_format($tc_y) . "y:";
        }
        if ($timecount > $tc_day) {
            $tc_d      = floor($timecount / $tc_day);
            $timecount = $timecount - ($tc_day * $tc_d);
            $TotalCountEcho .= number_format($tc_d) . "d:";
        }
        if ($timecount > $tc_hour) {
            $tc_h      = floor($timecount / $tc_hour);
            $timecount = $timecount - ($tc_hour * $tc_h);
            $TotalCountEcho .= number_format($tc_h) . "h:";
        }
        if ($timecount > $tc_minutes) {
            $tc_m      = floor($timecount / $tc_minutes);
            $timecount = $timecount - ($tc_minutes * $tc_m);
            $TotalCountEcho .= number_format($tc_m) . "m:";
        }
        $TotalCountEcho .= number_format($timecount) . "s";
        return $TotalCountEcho;
    }

    /**
     * @function login
     * this is our login function
     *
     * @param mixed $username
     * @param mixed $password
     *
     * @return bool
     */
    function login($username, $password)
    {
        $this->db->select('id, username, password');
        $this->db->from('users');
        $this->db->where('username', $username);
        //protected
        $this->db->where('password', MD5($password));
        //unprotected
        //$this -> db -> where('password', $password);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @function checkIfExists
     * this will allow us to check if a player already exisit in our DB
     *
     * @param mixed $field
     * @param mixed $value
     *
     * @return bool
     */
    function checkIfExists($field, $value)
    {
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where($field, $value);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @function percent
     * This will calculate the percent between
     * two amounts.
     *
     * @param mixed $num_amount
     * @param mixed $num_total
     *
     * @return float
     */
    function percent($num_amount, $num_total)
    {
        $count1 = $num_amount / $num_total;
        $count2 = $count1 * 100;
        return $count2;
    }

    /**
     * @function _get_next_rank_amount
     * This will give us the next rank amount
     *
     * @param mixed $rep_points
     *
     * @return mixed
     */
    function _get_next_rank_amount($rep_points)
    {
        if ($rep_points < 1000) {
            return 1000;
        } elseif ($rep_points < 2000) {
            return 2000;
        } elseif ($rep_points < 4000) {
            return 4000;
        } elseif ($rep_points < 8000) {
            return 8000;
        } elseif ($rep_points < 16000) {
            return 16000;
        } elseif ($rep_points < 32000) {
            return 32000;
        } elseif ($rep_points < 64000) {
            return 64000;
        } elseif ($rep_points < 128000) {
            return 128000;
        } elseif ($rep_points < 256000) {
            return 256000;
        } elseif ($rep_points < 512000) {
            return 512000;
        } elseif ($rep_points < 1024000) {
            return 1024000;
        } elseif ($rep_points < 2048000) {
            return 2048000;
        } elseif ($rep_points < 4096000) {
            return 4096000;
        } elseif ($rep_points < 8192000) {
            return 8192000;
        } elseif ($rep_points < 16384000) {
            return 16384000;
        } else {
            return 999999999999;
        }
    }

    function getuserlogfile($ip)
    {
        $query = $this->db->query('SELECT vps_ip,text,last_edit,vps_id,temp_text FROM logfile WHERE vps_ip = ? LIMIT 1', array($ip));
        if ($query->num_rows > 0) {
            $result = $query->result();
            foreach ($result as $row) {

                $hdfiles_array = array(
                    'vps_ip'    => $row->vps_ip,
                    'text'      => $row->text,
                    'last_edit' => $row->last_edit,
                    'vps_id'    => $row->vps_id

                );

                $logfile = $hdfiles_array;
                return $logfile;


            }
        }
    }

    /**
     * @function _format_rank_name
     * This function will format vps users rank
     * by checking how many reputation points
     *
     * @param mixed $rank
     *
     * @return string
     */
    function _format_rank_name($rep_points)
    {
        if ($rep_points < 1000) {
            return ' Script kiddie';
        } elseif ($rep_points < 2000) {
            return ' Spammer';
        } elseif ($rep_points < 4000) {
            return ' Cyberpunk';
        } elseif ($rep_points < 8000) {
            return ' Exploiter';
        } elseif ($rep_points < 16000) {
            return ' Geek';
        } elseif ($rep_points < 32000) {
            return ' Hardware modifier';
        } elseif ($rep_points < 64000) {
            return ' Virus programmer';
        } elseif ($rep_points < 128000) {
            return ' Highly skilled programmer';
        } elseif ($rep_points < 256000) {
            return ' Coder';
        } elseif ($rep_points < 512000) {
            return ' Crackbie';
        } elseif ($rep_points < 1024000) {
            return ' Cracker';
        } elseif ($rep_points < 2048000) {
            return ' DDoS junkie';
        } elseif ($rep_points < 4096000) {
            return ' Hacking genius';
        } elseif ($rep_points < 8192000) {
            return ' Admin\'s nightmare';
        } elseif ($rep_points < 16384000) {
            return ' Computer security expert';
        } else {
            return ' The architect';
        }
    }

    /**
     * @function _format_bytes
     *
     * This function will format the size according to normal standards
     *
     * @param mixed $a_bytes
     *
     * @return string
     */
    function _format_bytes($a_bytes)
    {
        if ($a_bytes < 1024) {
            return $a_bytes . ' B';
        } elseif ($a_bytes < 1048576) {
            return round($a_bytes / 1024, 3) . ' KiB';
        } elseif ($a_bytes < 1073741824) {
            return round($a_bytes / 1048576, 3) . ' MiB';
        } elseif ($a_bytes < 1099511627776) {
            return round($a_bytes / 1073741824, 3) . ' GiB';
        } elseif ($a_bytes < 1125899906842624) {
            return round($a_bytes / 1099511627776, 3) . ' TiB';
        } elseif ($a_bytes < 1152921504606846976) {
            return round($a_bytes / 1125899906842624, 3) . ' PiB';
        } elseif ($a_bytes < 1180591620717411303424) {
            return round($a_bytes / 1152921504606846976, 3) . ' EiB';
        } elseif ($a_bytes < 1208925819614629174706176) {
            return round($a_bytes / 1180591620717411303424, 3) . ' ZiB';
        } else {
            return round($a_bytes / 1208925819614629174706176, 3) . ' YiB';
        }
    }

    function genUniqueTxt($n)
    {
        for ($i = 0; $i < $n; $i++) {
            $str = '';
            if (rand(0, 1)) {
                $str .= chr(rand(ord('A'), ord('Z')));
            } else {
                $str .= chr(rand(ord('0'), ord('9')));
            }
        }
        return $str;
    }

    /**
     * @function getusedspace
     * This will get the used space from selected vps
     *
     * @param mixed $vpsid
     *
     * @return int
     */
    function gethdusedspace($vpsid, $type = 0)
    {
        $this->db->select('size');
        $this->db->from('files');
        $this->db->where('vps_id', $vpsid);
        $this->db->where('hhd_type', $type);


        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result     = $query->result();
            $totalbytes = 0;
            foreach ($result as $row) {
                $totalbytes += $row->size;
            }
            return $totalbytes;
        } else {
            $totalbytes = 1;
            return $totalbytes;
        }
    }

    function checkifinfolder($fileid)
    {
        $this->db->select('id,is_folder,folder_id');
        $this->db->from('files');
        $this->db->where('id', $fileid);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($row->folder_id != 0) {
                return true;
            } else {
                return false;
            }
        }

    }

    function getfilesupload($id, $fileid, $type = 0, $loc_net = 0)
    {
        $this->db->select('id, vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl, ext, hhd_type,is_folder,folder_id');
        $this->db->from('files');
        $this->db->where('vps_id', $id);


        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->result();
            $list   = '';
            foreach ($result as $row) {

                $hdfiles_array = array(
                    'id'        => $row->id,
                    'filename'  => $row->filename,
                    'folder_id' => $row->folder_id,
                    'ext'       => $row->ext,
                    'size'      => $row->size,
                    'level'     => $row->level,
                    'type'      => $row->type
                );


                $files = $hdfiles_array;
                if ($files['type'] == 0) {
                    $list .= '';
                } else {
                    $list .= "<option value=" . $files['id'] . ">" . $files['ext'] . " | " . $files['level'] . " [ " . $files['filename'] . $files['ext'] . " ] ( " . $this->_format_bytes($files['size']) . " )</option>";
                }

            }
            return $list;
        }
    }

    function getfolders($id, $fileid, $type = 0)
    {
        $this->db->select('id, vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl, ext, hhd_type,is_folder,folder_id');
        $this->db->from('files');
        $this->db->where('type', 0);
        $this->db->where('hhd_type', $type);
        $this->db->where('vps_id', $id);


        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->result();
            if ($this->checkifinfolder($fileid)) {
                $list = "<option value=root>/</option>";
            } else {
                $list = '';
            }
            $hdfiles_array = array();
            foreach ($result as $row) {

                $hdfiles_array = array(
                    'id'        => $row->id,
                    'filename'  => $row->filename,
                    'folder_id' => $row->folder_id
                );


                $files = $hdfiles_array;

                $list .= "<option value=" . $files['id'] . ">" . $files['filename'] . "</option>";

            }
            return $list;
        }
    }

    /**
     * @function getharddrivefiles
     * This will list all software on selected vps
     *
     * @param mixed $vpsid
     *
     * @return string
     */
    function getharddrivefiles($vpsid, $type = 0, $folder_id = 0, $remote = 0, $C_vpsid = 0)
    {
        $this->db->select('id, vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl, ext, hhd_type,is_folder,folder_id');
        $this->db->from('files');
        $this->db->where('vps_id', $vpsid);
        $this->db->where('hhd_type', $type);
        $this->db->where('folder_id', $folder_id);


        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result        = $query->result();
            $list          = '';
            $hdfiles_array = array();
            foreach ($result as $row) {

                $hdfiles_array = array(
                    'id'         => $row->id,
                    'type'       => $row->type,
                    'image'      => $row->image,
                    'filename'   => $row->filename,
                    'level'      => $row->level,
                    'size'       => $row->size,
                    'last_edit'  => $row->last_edit,
                    'hidden'     => $row->hidden,
                    'hidden_lvl' => $row->hidden_lvl,
                    'ext'        => $row->ext,
                    'is_folder'  => $row->is_folder,
                    'hhd_type'   => $row->hhd_type,
                    'folder_id'  => $row->folder_id
                );


                $files           = $hdfiles_array;
                $hdaction_config = $this->config->item('slavehack');
                if ($files['hhd_type']) {
                    $controller = 'softwareext';
                } else {
                    $controller = 'software';
                }
                if ($remote == 1) {
                    $controller = 'connect';
                    $vpsid      = $C_vpsid;
                }
                if ($this->quejob->getsoftlevel($vpsid, '.skr') >= $files['hidden_lvl']) {

                    $files['action'] = "";
                    if ($hdaction_config['action'][$files['type']]) {
                        $files['action'] = "<a href=" . base_url($controller . "/" . $hdaction_config['action'][$files['type']] . $files['id']) . ">";
                    } else {
                        $files['action'] = "";
                    }

                    $list .= "<td bgcolor=#000000>" . $files['action'] . "<img src=" . base_url($files['image']) . " border=0></a>";

                    if ($files['is_folder'] == 1) {
                        $list .= "<td><a href=" . base_url("" . $controller . "/openFolder/" . $files['id'] . "") . ">" . $files['filename'] . $files['ext'] . "</a>";
                    } else {
                        $list .= " <td> " . $files['filename'] . $files['ext'] . "";
                    }

                    $list .= " <td>" . $files['level'] . "
        <td> " . $this->_format_bytes($files['size']) . "
        <td>" . date("F j, Y, g:i a", $files['last_edit']) . "
        <td bgcolor=#000000 width=15>
        <center><a href=" . base_url("" . $controller . "/delete/" . $files['id'] . "") . "><img src=" . base_url("assets/layout/delete.jpg") . " border=0></a>
        <td bgcolor=#000000 width=30>
        <small>" . $files['hidden_lvl'] . "</small>";
                    if ($this->quejob->getsoftlevel($vpsid, '.hdr') > 0) {
                        $list .= "<a href=" . base_url("" . $controller . "/hide/" . $files['id'] . "") . "><img src=" . base_url("assets/layout/hide.jpg") . " border=0></a>";

                    } else {
                        $list .= "<img src=" . base_url("assets/layout/hide.jpg") . " border=0>";

                    }
                    if ($files['hhd_type'] && $files['is_folder'] == 0 || $remote == 1) {
                        $list .= "<td bgcolor=#000000 width=15><a href=" . base_url("" . $controller . "/download/" . $files['id'] . "") . "><img src=" . base_url("assets/layout/copy.jpg") . " border=0 alt=Download to vps</a>";

                    } else {
                        $list .= '<td bgcolor=#000000 width=15>';
                    }
                    if ($files['is_folder'] == 0) {
                        $list .= "<td bgcolor=#000000 width=15><a href=" . base_url("" . $controller . "/move/" . $files['id'] . "") . "><small>move</small></a>
        <tr bgcolor=#333333>";
                    } else {
                        $list .= "<td bgcolor=#000000 width=15><tr bgcolor=#333333>";
                    }
                }
            }
            return $list;
        } else {
            $list = "<td colspan=9 align=center>Hard disk / Folder is currently empty";
            return $list;
        }

    }

    function insertnewlogfile($username)
    {
        $result = $this->user->userinfo($username);

        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $newlog = array(
                    'vps_id'    => $row->id,
                    'text'      => 'Welcome to slavehack',
                    'temp_text' => 'Welcome to slavehack',
                    'last_edit' => time(),
                    'vps_ip'    => $row->vpsip

                );
                if ($this->db->insert('logfile', $newlog)) {
                    return true;
                } else {
                    return false;
                }
            }
        }


    }

    /**
     * @function register
     * This function will register selected user with valid details
     *
     * @param mixed $username
     * @param mixed $password
     * @param mixed $email
     *
     * @return bool
     */
    function register($username, $password, $email)
    {
        //begin register code need to do,,
        //instert user in db give IP adress and new vps set all defult values
        //instert logfile, harddrive(for more then harddrive maybe) hardware ect ect
        //build new data array
        $newuser = array(
            'username'    => $username,
            'email'       => $email,
            'password'    => md5($password),
            'date_joined' => time(),
            'vpspass'     => $this->genUniqueTxt(7),
            'vpsip'       => rand(15, 255) . "." . rand(0, 255) . "." . rand(0, 255) . "." . rand(0, 255)
        );
        // instert status  + new array of data +new log file
        if ($this->db->insert('users', $newuser)) {

            if ($this->insertnewlogfile($username)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        };

    }

    /**
     * @function userinfo
     * This function pull any userinfo we want from the db
     *
     * @param mixed $username
     *
     * @return bool
     */
    function userinfo($username)
    {
        $query = $this->db->query('SELECT * FROM users WHERE username = ? LIMIT 1', array($username));
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @function checkfolderexist
     * This will check to make sure folder exist otherwise return false
     *
     * @param $folder_id
     *
     * @return bool
     */
    function checkfolderexist($vpsid,$folder_id){
        $this->db->select('id, vps_id, type, image, filename, level, size, last_edit, hidden, hidden_lvl, ext, hhd_type,is_folder,folder_id');
        $this->db->from('files');
        $this->db->where('vps_id', $vpsid);
        $this->db->where('id', $folder_id);
        $this->db->where('is_folder', 1);


        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }


}


