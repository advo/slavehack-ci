<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 session_start();
class Internet extends CI_Controller {

    /**
     * About Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/about
     *    - or -  
     *         http://example.com/index.php/about/index
     *    - or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/about/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */  
     
    public function index()
    {
       
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
        $result = $this->user->userinfo($session_data['username']);

        if($result)
        {
        $sess_array = array();
        foreach($result as $row)
        {
            $sess_array = array(
            'id' => $row->id,
            'username' => $row->username,
            'vpspass' => $row->vpspass,
            'vpsip' => $row->vpsip,
            'date_joined2' => time() - $row->date_joined,
            'date_joined' => $row->date_joined,
            'hardrive_size' => $row->hardrive_size,
            'url' => $row->homepage
            );
            $data=$sess_array;
        }
   }
        /**
        * Lets ping vps and if true dhow the ping info
        * then call type of vps so we can load special veiws
        * or just normal webservice
        * 
        * @var Internet
        */
          
         if ($this->input->post('var2')) 
         {
           $data['ping_test'] = $this->quejob->pinginfo($this->input->post('var2'),$data['vpsip']);
           $data['url'] = $this->input->post('var2');
            switch($this->quejob->getvpstpye($data['url']))
           {
           case 0:
           $data['web_info'] = $this->quejob->webinfo($data['url']);
           $this->load->view('ingame/topIN',$data); 
           $this->load->view('internet',$data);
           $this->load->view('ingame/bottomIN',$data);
           break;
           case 1:
           $data['web_info'] = $this->quejob->webinfo($data['url']);
           $this->load->view('topIN',$data); 
           $this->load->view('internet_bank',$data);
           $this->load->view('bottomIN',$data);
           break;
           }
            
         }
         else
         {
             if (!empty($_GET['ip'])) 
         {
           $data['url'] =  $_GET['ip']; 
         }
           $data['ping_test'] = $this->quejob->pinginfo($data['url'],$data['vpsip']);
            switch($this->quejob->getvpstpye($data['url']))
           {
           case 0:
           $data['web_info'] = $this->quejob->webinfo($data['url']);
           $this->load->view('ingame/topIN',$data); 
           $this->load->view('internet',$data);
           $this->load->view('ingame/bottomIN',$data);
           break;
           case 1:
           $data['web_info'] = $this->quejob->webinfo($data['url']);
           $this->load->view('ingame/topIN',$data); 
           $this->load->view('internet_bank',$data);
           $this->load->view('ingame/bottomIN',$data);
           break;
           }
           
         }
        
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
       
           
    }  
  
    public function login($ip ='0')
    {
       
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'url' => $ip,
         'connected_to' => $row->connected_to,
         'login_info' => 'Something wrong here'
       );
       $data=$sess_array;
     }
   }
       
        if ($data['connected_to'] == $ip) {
            redirect('connect/', 'refresh');  
        }elseif($this->quejob->checkisslaved($ip,$data['vpsip'])){
            if ($this->input->post('login')){
             $slavepassword = $this->input->post('login');
             //do login function will redirect and record in log file each login
             $this->quejob->dovpslogin($ip,$data['vpsip']);   
            }else{
             $slavepassword = $this->quejob->getcurrentpassword($ip);   
            }
           
            $data['login_info'] = ' <form action="" method="post">
        <table style="margin-left: auto;margin-right: auto;">
            <tbody>
                <tr>
                    <td>Username</td>
                    <td>Admin</td>
                </tr>
                <input type="hidden" name="login" value="'.$slavepassword.'"
                <tr>
                    <td>Password</td>
                    <td>'.$slavepassword.'</td>
                </tr>
                <tr>
                <td>
                    <input type="submit" name=submit value="Login">
                </td>
            </tr>
            </tbody>
        </table>
    </form>';
   }else
        {
        $data['login_info'] = ' <form action="" method="post">
        <table style="margin-left: auto;margin-right: auto;">
            <tbody>
                <tr>
                    <td>Username</td>
                    <td>Admin</td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>???</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </form>';
    $data['login_info'] .= '<small>Attempt to '.anchor('internet/crack/'.$data['url'].'',  'Crack this Password ', array('title' => 'Leet cracks ahead!')).' this password <br></small>  ';
  
        }
        $this->load->view('ingame/topIN',$data); 
        $this->load->view('internet_login',$data);
        $this->load->view('ingame/bottomIN',$data); 
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
       
           
    }  
 
 public function crack($ip, $done = 0)
 {
      if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'url' => $ip,
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent
       );
       $data=$sess_array;
     }
   } 
   //check user has waterwall programs to crack this     
     if($this->quejob->cancrack($ip,$data['id']))
     {
        if($done)
        {
            //this will show if cracked or not and show password and link to login auto 
            //check if vps is added as slave if not show failed
            if($this->quejob->checkisslaved($ip,$data['vpsip'])  )
            {
              $data['crakworkform']="Passsword Cracked<br>".anchor('connect/',  'Click to Continue ', array('title' => 'Continue to Connected PC!'));
            }
            else
            {
             $data['crakworkform']="Failed to crack pasword";    
            }
            
             $this->load->view('ingame/topIN',$data); 
            $this->load->view('internet_crack',$data);
            $this->load->view('ingame/bottomIN',$data);  
        }
        else  
        {
            //check if we are cracking a password if not start cracking or just show the process
            if (!$this->quejob->iscracking($data['id'],$ip)){
             $timespent = time()-$data['last_update'];
             if ($timespent > (60*120))
            {
            $newtimespent = $data['time_spent']+0; 
            }
            else
            {
            $newtimespent = $data['time_spent']+$timespent;
            }
            $this->quejob->instert_new_process($data['id'],2,$newtimespent,$ip); 
            }
            $data['crakworkform']=$this->quejob->getactiveprocesslistbytype($data['id'],2);
            $this->load->view('ingame/topIN',$data); 
            $this->load->view('internet_crack',$data);
            $this->load->view('ingame/bottomIN',$data);  
        }
     }     
     else
     {
     $data['crakworkform']= 'Can not bypass firewall get a better waterwall';
     $this->load->view('ingame/topIN',$data); 
     $this->load->view('internet_crack',$data);
     $this->load->view('ingame/bottomIN',$data); 
     
     }
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');  
   }
 }     
}