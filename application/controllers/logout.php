<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 session_start();
class Logout extends CI_Controller {

    /**
     * About Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/about
     *    - or -  
     *         http://example.com/index.php/about/index
     *    - or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/about/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */  
     
    public function index()
    {
       
      $this->session->unset_userdata('logged_in');
      session_destroy();
      redirect('', 'refresh');
      
           
    }  
    
      
}