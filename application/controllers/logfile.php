<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 session_start();
class Logfile extends CI_Controller {

    /**
     * About Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/about
     *    - or -  
     *         http://example.com/index.php/about/index
     *    - or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/about/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */  
     
    public function index()
    {
       
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent
         );
       $data=$sess_array;
     }
   }
   
        if ($this->input->post('editlog'))
        {
            $timespent = time()-$data['last_update'];
             if ($timespent > (60*120))
            {
            $newtimespent = $data['time_spent']+0; 
            }
            else
            {
            $newtimespent = $data['time_spent']+$timespent;
            }
            //update logfile or user
           $this->quejob->updatelogfiletemp($data['vpsip'],$this->input->post('editlog'));
           $this->quejob->instert_new_process($data['id'],3,$newtimespent,$data['vpsip']);  
           $logfile = $this->user->getuserlogfile($row->vpsip);
           $data['logfile_text'] = $logfile['text'];
           if ($this->quejob->getactiveprocesslistbytype($data['id'],3))
           {
           $data['working'] = $this->quejob->getactiveprocesslistbytype($data['id'],3);
           $this->load->view('ingame/topIN',$data); 
           $this->load->view('logfile_working',$data);
           $this->load->view('ingame/bottomIN',$data);
           }else{
           $logfile = $this->user->getuserlogfile($row->vpsip);
           $data['logfile_text'] = $logfile['text'];
           $this->load->view('ingame/topIN',$data); 
           $this->load->view('logfile',$data);
           $this->load->view('ingame/bottomIN',$data);   
           }
            
        }else{
           if ($this->quejob->getactiveprocesslistbytype($data['id'],3))
           {
           $data['working'] = $this->quejob->getactiveprocesslistbytype($data['id'],3);
           $this->load->view('ingame/topIN',$data); 
           $this->load->view('logfile_working',$data);
           $this->load->view('ingame/bottomIN',$data);
           }else{
           $logfile = $this->user->getuserlogfile($row->vpsip);
           $data['logfile_text'] = $logfile['text'];
           $this->load->view('ingame/topIN',$data); 
           $this->load->view('logfile',$data);
           $this->load->view('ingame/bottomIN',$data);   
           } 
        }
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
       
           
    }  
  
  
      
}