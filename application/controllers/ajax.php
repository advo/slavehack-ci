<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

    /**
     * Ajax controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/ajax
     *    - or -  
     *         http://example.com/index.php/ajax/index
     *    - or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/ajax/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
    
        
    }  
    
    public function recaptcha()
    {
    $this->recaptcha->recaptcha_check_answer($_SERVER["REMOTE_ADDR"],
    $this->input->post('recaptcha_challenge_field'),
    $this->input->post('recaptcha_response_field')
    );
 
     $id = $this->input->post('vpsid');
     $hash = $this->input->post('hash');;
     $query = $this->db->query('UPDATE users SET last_bot_check = ? WHERE id =? AND password = ? LIMIT 1',array(time(),$id,$hash));
    if ($this->recaptcha->is_valid) {
        die("success");   
        }
        else
        {
        die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
       "(reCAPTCHA said: " . $this->recaptcha->error . ")");
       } 
    }
 
}