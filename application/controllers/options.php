<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 session_start();
class Options extends CI_Controller {

    /**
     * About Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/about
     *    - or -  
     *         http://example.com/index.php/about/index
     *    - or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/about/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */  
     
    public function index()
    {
       
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size
       );
       $data=$sess_array;
     }
   }
        $this->load->view('ingame/topIN',$data); 
        $this->load->view('options',$data);
        $this->load->view('ingame/bottomIN',$data); 
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
       
           
    }  
  
  
      
}