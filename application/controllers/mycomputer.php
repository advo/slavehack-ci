<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 session_start();
class Mycomputer extends CI_Controller {

    /**
     * About Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/about
     *    - or -  
     *         http://example.com/index.php/about/index
     *    - or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/about/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */  
     
    public function index()
    {
       
       if($this->session->userdata('logged_in'))
       {
            $session_data = $this->session->userdata('logged_in');
            $result = $this->user->userinfo($session_data['username']);

            if($result)
            {
                $sess_array = array();
                foreach($result as $row)
                {
                    $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->username,
                    'password' => $row->password,
                    'vpspass' => $row->vpspass,
                    'vpsip' => $row->vpsip,
                    'date_joined2' => time() - $row->date_joined,
                    'date_joined' => $row->date_joined,
                    'hardrive_size' => $this->user->_format_bytes($row->hardrive_size),
                    'ip_resets' => $row->ip_resets,
                    'reputation_points' => $row->reputation,
                    'reputation_rank' => $this->user->_format_rank_name($row->reputation),
                    'next_reputation_rank' => $this->user->_get_next_rank_amount($row->reputation),
                    'rank_perc' => floor($this->user->percent($row->reputation,$this->user->_get_next_rank_amount($row->reputation))),
                    'last_update' => $this->user->TimeCount(time() - $row->last_update),
                    'time_spent' => $this->user->DisplayTotalTime($row->time_spent),
                    'random_tip' => $this->user->random_tip(),
                    'connected_to' =>  $this->quejob->conecctedtoinfo($row->vpsip),
                    'time_spent_crack' => $this->user->DisplayTotalTime($row->time_spent_crack),
                    'last_ip_reset' => $this->user->DisplayTotalTime(time() - $row->last_ip_reset),
                    'last_bot_check' => $row->last_bot_check 
                    );
                    $data=$sess_array;
                    $diff = $data['date_joined2'];

                    $years = floor($diff / (365*60*60*24));
                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                    $data['date_joined2']=   $years." years, ".$months." months, ".$days." days\n";
                }
            }
            $data['workform_mycomputer'] = false;
            $result1 = $this->quejob->get_active_process($data['id']);

            if($result1)
            {
                $sess_array1 = array();
                foreach($result1 as $row)
                {
                    $sess_array1 = array(
                    'id' => $row->id,
                    'time_started' => $row->time_started,
                    'time_finished' => $row->time_finished,
                    'type' => $row->type
                    );
                    $process=$sess_array1;
       
                }
               
                $mpc_config = $this->config->item('slavehack');
                $data['timeleft']= $process['time_finished'] -time(); 
                $data['workform_mycomputer'] = '
            <script type="text/javascript">
            var pressed_1 = false;
            var finished_1 = false;

            var date = new Date();
            var milisecStarted_1=date.getTime()-0;
            var milisecEnd_1=date.getTime()+'.$data['timeleft'].'*1000;
            var milisecTotalTime_1=milisecEnd_1-milisecStarted_1;

            function prog_1()
            {
                if (pressed_1 == false)
                {
                    pressed_1 = true;

                    progBar_1();
                }
            }
            function progBar_1()
            {
            if (finished_1 == false )
            {
                var now = new Date();
                milisecNow_1=now.getTime();

                var miliSecondsLeft_1=Math.ceil((milisecEnd_1-milisecNow_1)/100);
                if(miliSecondsLeft_1<0){miliSecondsLeft_1=0;}

                var hours_1=Math.floor(miliSecondsLeft_1/36000);
                var minutes_1=Math.floor(miliSecondsLeft_1%36000/600);
                var seconds_1=Math.floor(miliSecondsLeft_1%36000%600)/10;
                if((seconds_1%1)==0)
                {
                    seconds_1=seconds_1+".0";
                }
                var output_1;
                if(hours_1>0)
                {
                    var output_1=hours_1+" hours, "+minutes_1+" minutes and "+seconds_1+" seconds.";
                }
                else
                {
                    if(minutes_1>0)
                    {
                        var output_1=minutes_1+" minute(s) and "+seconds_1+" seconds.";
                    }
                    else
                    {
                        var output_1=seconds_1+" seconds.";
                    }
                }



                miliSecondsLeft_1=Math.floor(miliSecondsLeft_1)/10;
                var procent_1=Math.floor(((milisecTotalTime_1-miliSecondsLeft_1*1000)/milisecTotalTime_1)*100);
                if(procent_1<=0)
                {
                    procent_1=1;
                }
                if (milisecEnd_1>=milisecNow_1)
                {

                }
                else
                {
                    finished_1 = true;


                    document.getElementById("d3_1").innerHTML="<input class=formnew type=submit value=Continue>";
                    $("#WorkForm_'.$process['id'].'").submit();
                }        
               progressBar((procent_1), $(\'#progressBar\'));            
                setTimeout("progBar_1();", 100);    
            }
        }
        window.setTimeout("prog_1()",200);
        
        </script>    
        <form name=WorkForm_1 action=processes/finishtask/'.$process['id'].' id=WorkForm_'.$process['id'].'>
        <input type=hidden value=workscreen value=1> 
        <table class=titleproc style=margin-left: auto; margin-right: auto;>
        <tr>
        <td>
        <small class=specfont2>'.$mpc_config['process'][$process['type']].' - <a href=processes/kill/'.$process['id'].'>Cancel</a></small>
         <div id="progressBar" class="jquery-ui-like"><span class=specfont1><div></div></span></div>
         <div id=d3_1>
        </div>
        </td>
        </tr>
        </table>
        </form> 
        ';  
            }
            $hrlimit = time() - 900;
            if ($data['last_bot_check'] >= $hrlimit) {
            $data['botcheck']   = '';
            }else
            {
             $data['botcheck']   = $this->quejob->dobotcheck($data['id'],$data['password']);   
            }
            
            $this->load->view('ingame/topIN',$data); 
            $this->load->view('mycomputer',$data);
            $this->load->view('ingame/bottomIN',$data); 
        }
        else
        {    
            //If no session, redirect to login page
            redirect('', 'refresh');
        }
       
  }  
  
  function passreset()
  {
      //allways check user is still logged in if not we dont want people doing things
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
        $result = $this->user->userinfo($session_data['username']);
         $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent
       );
       $data=$sess_array;
     }
     
    $timespent = time()-$data['last_update'];
    if ($timespent > (60*120))
    {
     $newtimespent = $data['time_spent']+0; 
    }
    else
    {
    $newtimespent = $data['time_spent']+$timespent;
    }
    $this->quejob->instert_new_process($data['id'],0,$newtimespent);
    redirect('mycomputer', 'refresh');  
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
  }
  
    function ipreset()
  {
      //allways check user is still logged in if not we dont want people doing things
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
        $result = $this->user->userinfo($session_data['username']);
         $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent
       );
       $data=$sess_array;
     }
$timespent = time()-$data['last_update'];
    if ($timespent > (60*120))
    {
     $newtimespent = $data['time_spent']+0;   
    }
    else
    {
    $newtimespent = $data['time_spent']+$timespent;
    }
    $this->quejob->instert_new_process($data['id'],1,$newtimespent);
    redirect('mycomputer', 'refresh');  
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
  }
      
}