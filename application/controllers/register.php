<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

    /**
     * About Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/about
     *    - or -  
     *         http://example.com/index.php/about/index
     *    - or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/about/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
   
    public function index()
    {
    
    //This method will have the credentials validation
        
   $this->load->library('form_validation');
   //when doing multiple froms on one page 
   //make sure that the submit name is set otherwise it wont work
      if ($this->input->post('login')) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');    
        }
      if ($this->input->post('reg')) {
            $this->form_validation->set_rules('username2', 'Username', 'trim|required|xss_clean|min_length[6]|max_length[32]|callback_check_username');
            $this->form_validation->set_rules('password2', 'Password', 'trim|required|xss_clean|matches[passwordCheck]|min_length[6]|max_length[32]');
            $this->form_validation->set_rules('passwordCheck', 'Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_check_email');
            $this->form_validation->set_rules('akk', 'Rules', 'trim|required|callback_check_rules|min_length[2]|max_length[3]');  
    }     
     
   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
        $this->load->view('outgame/topOUT'); 
        $this->load->view('outgame/index_register');
        $this->load->view('outgame/bottomOUT');
   }
   else
   {   
       if ($this->input->post('login')) {  
     redirect('mycomputer', 'refresh');
       }else if   ($this->input->post('reg')) {  
        //register account and redirect to register success page
        if($this->user->register($this->input->post('username2'), $this->input->post('password2'), $this->input->post('email')))
        {
        $data['info']= "Register complete, activation is not needed at this time please login and test everything is working.";
        $this->load->view('outgame/topOUT'); 
        $this->load->view('outgame/index_register_suc');
        $this->load->view('outgame/bottomOUT');
        }
        else
        {
        $data['info']= "Register FAILED, A serious error has happen during the register process. Please inform the administation as soon as possible thank you very much.";
        $this->load->view('outgame/topOUT',$data); 
        $this->load->view('outgame/index_register_fail',$data);
        $this->load->view('outgame/bottomOUT',$data);  
        }   
       }
   }
        
    }   
    
   function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');

   //query the database
   $result = $this->user->login($username, $password);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 } 
 function check_username($username)
 {
     if($this->user->checkIfExists('username',$username))
     {
          $this->form_validation->set_message('check_username', 'Username is already taken');
          return false;
     }
     else
     {
         return true;
     }
 }
 
  function check_email($email)
 {
     if($this->user->checkIfExists('email',$email))
     {
          $this->form_validation->set_message('check_email', 'Email is already in use, you can only have 1 account please use <a href=forgot/ >Forgot password</a>.');
          return false;
     }
     else
     {
         return true;
     }
 }
 
 function check_rules($rules)
 {
  if($rules == "yes")
   {
         return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_rules', 'You must type yes in the accept rules text field');
     return false;
   }
 } 
 
 
  function forgot()
 {
     $data['gamename']="RAGE HACK";
        //This method will have the credentials validation
   $this->load->library('form_validation');

   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|callback_check_username');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
        $this->load->view('outgame/topOUT',$data); 
        $this->load->view('outgame/index_forgot',$data);
        $this->load->view('outgame/bottomOUT',$data);
   }
   else
   {
     //Go to private area
     redirect('mycomputer', 'refresh');
   }
 } 
}