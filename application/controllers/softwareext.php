<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 session_start();
class Softwareext extends CI_Controller {

    /**
     * About Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/about
     *    - or -  
     *         http://example.com/index.php/about/index
     *    - or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/about/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */  
     
    public function index()
    {
       
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
     
     $data['filelist']=$this->user->getharddrivefiles($data['id'],1);
     $data['totalusedspace'] = $this->user->gethdusedspace($data['id'],1);
     $data['totalfreespace'] = $data['ext_harddrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext',$data);
        $this->load->view('ingame/bottomIN',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
       
           
    }
    
    
     public function openfolder($folder_id)
    {
       
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'last_ip_reset' => $row->last_ip_reset,
         'folder_id' => $folder_id,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
     
     $data['filelist']=$this->user->getharddrivefiles($data['id'],1,$folder_id);
     $data['totalusedspace'] = $this->user->gethdusedspace($data['id'],1);
     $data['totalfreespace'] = $data['ext_harddrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_openfolder',$data);
        $this->load->view('ingame/bottomIN',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
       
           
    }
    
    public function  move($fileid)
 {
     if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'last_ip_reset' => $row->last_ip_reset,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
     
     $data['totalusedspace'] = $this->user->gethdusedspace($data['id'],1);
     $data['totalfreespace'] = $data['ext_harddrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
     if($this->input->post('moveTo')){
         $moveTo = $this->input->post('moveTo');
         if ($moveTo == 'root'){
          $moveTo =0;   
         }
         $timespent = time()-$data['last_update'];
             if ($timespent > (60*120))
            {
            $newtimespent = $data['time_spent']+0; 
            }
            else
            {
            $newtimespent = $data['time_spent']+$timespent;
            }
         $this->quejob->instert_new_process($data['id'],9,$newtimespent,$data['vpsip'],$filename = 'unnamed',$file_text='blank',$this->input->post('moveTo'),$fileid);  
        $data['folders_work'] = $this->quejob->getactiveprocesslistbytype($data['id'],9);;
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_move_work',$data);
        $this->load->view('ingame/bottomIN',$data);   
     }else{
        $data['folders'] = $this->user->getfolders($data['id'],$fileid,1);
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_move',$data);
        $this->load->view('ingame/bottomIN',$data);
     }
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   } 
 }
    public function create($type = 'file',$folder_id =0)
    {
    if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size,
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
     
         switch($type){
         case 'file':
        if (($this->input->post('filename')) && ($this->input->post('data')))
        {
            $timespent = time()-$data['last_update'];
             if ($timespent > (60*120))
            {
            $newtimespent = $data['time_spent']+0; 
            }
            else
            {
            $newtimespent = $data['time_spent']+$timespent;
            }
         if ($folder_id != 0)
        {
         $this->quejob->instert_new_process($data['id'],6,$newtimespent,$data['vpsip'],$this->input->post('filename'),$this->input->post('data'),$folder_id);     
        }
        else
        {
        $this->quejob->instert_new_process($data['id'],6,$newtimespent,$data['vpsip'],$this->input->post('filename'),$this->input->post('data'));  
          
        }
        $data['create_file_work'] =$this->quejob->getactiveprocesslistbytype($data['id'],6);
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_create_file_work',$data);
        $this->load->view('ingame/bottomIN',$data);  
        }
        else
        {
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_create_file',$data);
        $this->load->view('ingame/bottomIN',$data); 
        }
        
            break;
            case 'folder':
            if ($this->input->post('foldername'))
        {
            $timespent = time()-$data['last_update'];
             if ($timespent > (60*120))
            {
            $newtimespent = $data['time_spent']+0; 
            }
            else
            {
            $newtimespent = $data['time_spent']+$timespent;
            }
        $this->quejob->instert_new_process($data['id'],7,$newtimespent,$data['vpsip'],$this->input->post('foldername'));  
        $data['create_file_work'] = $this->quejob->getactiveprocesslistbytype($data['id'],7);
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_create_folder_work',$data);
        $this->load->view('ingame/bottomIN',$data);  
        }
        else
        {
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_create_folder',$data);
        $this->load->view('ingame/bottomIN',$data); 
        }
         break;
         }
       
         
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
        
    }  
  
 public function  hide($fileid)
 {
     if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'last_ip_reset' => $row->last_ip_reset,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
     
     $data['totalusedspace'] = $this->user->gethdusedspace($data['id'],1);
     $data['totalfreespace'] = $data['ext_harddrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
         if ($data['hide_lvl']  == 0)
        {
        $data['hide_work'] = '<center>You dont have any file hiding software.</center>';
        $this->load->view('ingame/topIN',$data);
        $this->load->view('software_hide',$data);
        $this->load->view('ingame/bottomIN',$data);   
        } 
        else
        {
        $timespent = time()-$data['last_update'];
        if ($timespent > (60*120))
        {
            $newtimespent = $data['time_spent']+0; 
        }
        else
        {
            $newtimespent = $data['time_spent']+$timespent;
        }
        $this->quejob->instert_new_process($data['id'],11,$newtimespent,$data['vpsip'],$filename = 'unnamed',$file_text='blank',0,$fileid);      
        $data['hide_work'] = $this->quejob->getactiveprocesslistbytype($data['id'],11);
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_hide',$data);
        $this->load->view('ingame/bottomIN',$data);
        }
        
     
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   } 
 } 
  
  public function delete($fileid)
 {
     if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'last_ip_reset' => $row->last_ip_reset,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
     
     $data['totalusedspace'] = $this->user->gethdusedspace($data['id']);
     $data['totalfreespace'] = $data['hardrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
        
        $timespent = time()-$data['last_update'];
        if ($timespent > (60*120))
        {
            $newtimespent = $data['time_spent']+0; 
        }
        else
        {
            $newtimespent = $data['time_spent']+$timespent;
        }
         if (intval($fileid) != 0)
        {
        $this->quejob->instert_new_process($data['id'],13,$newtimespent,$data['vpsip'],$filename = 'unnamed',$file_text='blank',0,$fileid);      
        $data['delete_work'] = $this->quejob->getactiveprocesslistbytype($data['id'],13);
            
        }
        else
        {
            $data['delete_work'] = 'OH MY YOU BROKE IT';        
        }
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_delete',$data);
        $this->load->view('ingame/bottomIN',$data);
        
        
     
 }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   } 
 } 
 
  public function upload()
    {
       
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
      $data['totalusedspace'] = $this->user->gethdusedspace($data['id'],1);
     $data['totalfreespace'] = $data['ext_harddrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
     if ($this->input->post('upload'))
     {
       
         
        $timespent = time()-$data['last_update'];
        if ($timespent > (60*120))
        {
            $newtimespent = $data['time_spent']+0; 
        }
        else
        {
            $newtimespent = $data['time_spent']+$timespent;
        }
        $this->quejob->instert_new_process($data['id'],14,$newtimespent,$data['vpsip'],$filename = 'unnamed',$file_text='blank',0,$this->input->post('upload'));      
        $data['delete_work'] = $this->quejob->getactiveprocesslistbytype($data['id'],14);
       $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_upload_work',$data);
        $this->load->view('ingame/bottomIN',$data);
     }else{
     $data['filelist']=$this->user->getfilesupload($data['id'],0,1,0);
    
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_upload',$data);
        $this->load->view('ingame/bottomIN',$data);
     }
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
       
           
    }  
    
    public function download($file_id)
    {
       
       if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
      $data['totalusedspace'] = $this->user->gethdusedspace($data['id'],1);
     $data['totalfreespace'] = $data['ext_harddrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
     if ($file_id != 0)
     {
       
         
        $timespent = time()-$data['last_update'];
        if ($timespent > (60*120))
        {
            $newtimespent = $data['time_spent']+0; 
        }
        else
        {
            $newtimespent = $data['time_spent']+$timespent;
        }
        //make a check to see if file exisit and also if remote check conected ip is the ip belonging to file
        $this->quejob->instert_new_process($data['id'],15,$newtimespent,$data['vpsip'],$filename = 'unnamed',$file_text='blank',0,$file_id);      
        $data['download_work'] = $this->quejob->getactiveprocesslistbytype($data['id'],15);
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_download',$data);
        $this->load->view('ingame/bottomIN',$data);
     }
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   }
       
           
  }
  
  public function  format()
 {
     if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'last_ip_reset' => $row->last_ip_reset,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
     
     $data['totalusedspace'] = $this->user->gethdusedspace($data['id'],1);
     $data['totalfreespace'] = $data['ext_harddrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
        
        $timespent = time()-$data['last_update'];
        if ($timespent > (60*120))
        {
            $newtimespent = $data['time_spent']+0; 
        }
        else
        {
            $newtimespent = $data['time_spent']+$timespent;
        }
       
        $this->quejob->instert_new_process($data['id'],17,$newtimespent,$data['vpsip'],$filename = 'unnamed',$file_text='blank',0,0);      
        $data['format_work'] = $this->quejob->getactiveprocesslistbytype($data['id'],17);
            
        
        $this->load->view('ingame/topIN',$data);
        $this->load->view('ingame/softwareext/softwareext_format',$data);
        $this->load->view('ingame/bottomIN',$data);
        
        
     
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   } 
 }
  public function  editfile($fileid)
 {
     if($this->session->userdata('logged_in'))
   {
        $session_data = $this->session->userdata('logged_in');
         $result = $this->user->userinfo($session_data['username']);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'vpspass' => $row->vpspass,
         'vpsip' => $row->vpsip,
         'date_joined2' => time() - $row->date_joined,
         'date_joined' => $row->date_joined,
         'hardrive_size' => $row->hardrive_size,
         'ext_harddrive_size' => $row->external_hd_size, 
         'last_update' => $row->last_update,
         'time_spent' =>$row->time_spent,
         'last_ip_reset' => $row->last_ip_reset,
         'seek_lvl' => $this->quejob->getsoftlevel($row->id,'.skr'),
         'hide_lvl' => $this->quejob->getsoftlevel($row->id,'.hdr')
       );
       $data=$sess_array;
     }
   }
     
     $data['totalusedspace'] = $this->user->gethdusedspace($data['id'],1);
     $data['totalfreespace'] = $data['ext_harddrive_size'] - $data['totalusedspace'];
     $data['bar_maxperc'] =  ($data['totalfreespace']) / ($data['totalusedspace'] ) *100;
     $data['bar_minperc'] = ($data['totalusedspace']) / ($data['totalfreespace'] ) *100;
        
       if ($this->input->post('data'))
        {
            $timespent = time()-$data['last_update'];
             if ($timespent > (60*120))
            {
            $newtimespent = $data['time_spent']+0; 
            }
            else
            {
            $newtimespent = $data['time_spent']+$timespent;
            }

        $this->quejob->instert_new_process($data['id'],19,$newtimespent,$data['vpsip'],'filename',$this->input->post('data'),0,$fileid);  
          
        $data['editfile_work'] =$this->quejob->getactiveprocesslistbytype($data['id'],19);
        $this->load->view('ingame/topIN',$data); 
        $this->load->view('ingame/softwareext/softwareext_editfile_work',$data);
        $this->load->view('ingame/bottomIN',$data);   
        }
        else
        {
        $file_infoq =$this->quejob->getfileinfo($fileid);   
        $data['file_info'] = $file_infoq[0];
        //var_dump($data['file_info'][0]);die();exit();
        $this->load->view('ingame/topIN',$data); 
        $this->load->view('ingame/softwareext/softwareext_editfile',$data);
        $this->load->view('ingame/bottomIN',$data);  
        }
        
        
     
   }
   else
   {
     //If no session, redirect to login page
     redirect('', 'refresh');
   } 
 }          
}