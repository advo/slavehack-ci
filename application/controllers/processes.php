<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class processes extends CI_Controller {

    /**
     * processes Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/processes/index
     *         http://example.com/processes/finishtask
     *         http://example.com/processes/kill
     *    
     *

     */  
     
    public function index()
    {
       
       if($this->session->userdata('logged_in'))
       {
        $session_data = $this->session->userdata('logged_in');
        $result = $this->user->userinfo($session_data['username']);

                if($result)
                {
                    $sess_array = array();
                    foreach($result as $row)
                    {
                        $sess_array = array(
                        'id' => $row->id,
                        'username' => $row->username,
                        'vpspass' => $row->vpspass,
                        'vpsip' => $row->vpsip,
                        'date_joined2' => time() - $row->date_joined,
                        'date_joined' => $row->date_joined
                        );
                        $data=$sess_array;
                    }
                }
        $data['conectedto'] = $this->quejob->conecctedtoinfo($data['vpsip']);
        $data["process_list"] = $this->quejob->getactiveprocesslist($data['id']);
   
        $this->load->view('ingame/topIN',$data); 
        $this->load->view('processes',$data);
        $this->load->view('ingame/bottomIN',$data); 
        }
            else
        {
             //If no session, redirect to login page
             redirect('', 'refresh');
        }
       
           
    }     
    
    function kill($id)
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $result = $this->user->userinfo($session_data['username']);

            if($result)
            {
                $sess_array = array();
                foreach($result as $row)
                {
                    $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->username,
                    'vpspass' => $row->vpspass,
                    'vpsip' => $row->vpsip,
                    'date_joined2' => time() - $row->date_joined,
                    'date_joined' => $row->date_joined
                    );
                    $data=$sess_array;
                }
            }
            
            $this->quejob->kill_process($id, $data['id']);
            redirect('processes', 'refresh');     
        }
            else
        {
            //If no session, redirect to login page
            redirect('', 'refresh');
        } 
    }

    function finishtask($processid)
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $result = $this->user->userinfo($session_data['username']);

            if($result)
                {
                    $sess_array = array();
                    foreach($result as $row)
                    {
                        $sess_array = array(
                        'id' => $row->id,
                        'username' => $row->username,
                        'vpspass' => $row->vpspass,
                        'vpsip' => $row->vpsip,
                        'date_joined2' => time() - $row->date_joined,
                        'date_joined' => $row->date_joined
                        );
                        $data=$sess_array;
                    }
                }
            // do finish task function
            $this->quejob->finish_active_process($processid,0,$data['id']);  
           
        }
            else
        {
            //If no session, redirect to login page
            redirect('', 'refresh');
        }  
    }
      
}