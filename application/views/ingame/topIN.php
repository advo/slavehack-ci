<!-- 1 --><!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>

<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
  <head>
    <title><?php echo GAME_NAME;?></title>
    <meta http-equiv='content-type' content='text/html;charset=utf-8' />
    <meta http-equiv='Content-Style-Type' content='text/css' />
    <link rel='stylesheet' type='text/css' href=<?php echo base_url("/assets/templates/layout1-1.css");?> />
    <link rel='stylesheet' type='text/css' href=<?php echo base_url("/assets/templates/theme_green.css"); ?> />

  <script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>  

<script src=<?php echo base_url("/assets/js.php?"); ?>></script>
    <script type="text/javascript" src="<?php echo base_url("/assets/js/progressbar.js"); ?>"></script>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url("/assets/css/skins/jquery-ui-like/progressbar.css");?>">  
  </head>
  <body>
    <!-- HEADER -->
    <style>
    #header {
background: url('<?php echo base_url("/assets/layout/static/headerBG_green.png");?>') repeat-x top left;
}
</style>
    <div id='header'>

      <a href='index2.php'>
      <img src=<?php echo base_url("/assets/layout/static/logo_green.png");?> style='width:700px; height:161px;' alt='Slavehack: The extended version' /></a>
      
    </div>
    <!-- /HEADER -->

    <!-- WRAPPER -->
    <div id='wrapper'>
      <!-- MAIN NAV -->
      <div id='subContent'>

        <h2>Navigation</h2>

        <div id='navImages'>

            <a href='<?php echo base_url("mycomputer");?>'>
            <img src='<?php echo base_url("/assets/layout/static/buttons/green/mycomputer.png");?>' alt='My computer' />
            </a><br />

            <a href='<?php echo base_url("processes");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/processes.png");?> alt='Processes' />
            </a><br />

            <a href='<?php echo base_url("hardware");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/hardware.png");?> alt='Hardware' />
            </a><br />

            <a href='<?php echo base_url("software");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/software.png");?> alt='Software' />
            </a><br />

            <a href='<?php echo base_url("logfile");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/logs.png");?> alt='Logs' />
            </a><br />

            <a href='<?php echo base_url("internet");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/internet.png");?> alt='Internet' />
            </a><br />

            <a href='<?php echo base_url("finances");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/finances.png");?> alt='Finances' />
            </a><br />

            <a href='<?php echo base_url("slaves");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/slaves.png");?> alt='Slaves' />
            </a><br />

            <a href='<?php echo base_url("email");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/email.png");?> alt='Email' />
            </a><br /><br />

            <a href='<?php echo base_url("options");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/options.png");?> alt='Options' />
            </a><br />

            <a href='<?php echo base_url("rules");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/rules.png");?> alt='Rules' />
            </a><br />

            <a href='<?php echo base_url("help");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/help.png");?> alt='Help' />
            </a><br />

            <a href='<?php echo base_url("forum");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/forum.png");?> alt='Forum' />
            </a><br />

            <a href='<?php echo base_url("highscore");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/highscores.png");?> alt='Highscores' />
            </a><br />

            <a href='<?php echo base_url("support");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/support.png");?> alt='Support' />
            </a><br /><br />

            <a href='<?php echo base_url("logout");?>'>
            <img src=<?php echo base_url("/assets/layout/static/buttons/green/logout.png");?> alt='Logout' />
            </a>

        </div>

      </div>
      <!-- /MAIN NAV -->