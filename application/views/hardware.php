<!-- CONTENT -->
     <div id='content'>
    <h2>[<?php echo $username;?>
    @<span onclick="this.innerHTML='<?php echo $vpsip;?>'"><small>click to show</small></span>] - TIME NOW</h2>
    <div id='container'>
        <h1>Hardware</h1>
        <table>
        <tbody>
        <tr>
            <td>
                <a href="<?php echo base_url("hardware/cpu");?>"><img src="<?php echo base_url("/assets/layout/upgrade.jpg");?>" border="0"></a> Cpu speed:
            </td>
            <td>
                2800 Mhz.<br>
            </td>
        </tr>
        <tr>
            <td>
                <a href="<?php echo base_url("hardware/connection");?>"><img src="<?php echo base_url("/assets/layout/upgrade.jpg");?>" border="0"></a> Connection speed:
            </td>
            <td>
                 8 Mbit (+- 910 Kb/s download, 228 Kb/s upload )<br>
            </td>
        </tr>
        <tr>
            <td>
                <a href="<?php echo base_url("hardware/hd");?>"><img src="<?php echo base_url("/assets/layout/upgrade.jpg");?>" border="0"></a> Harddrive:
            </td>
            <td>
                30 Gb capacity
            </td>
        </tr>
        <tr>
            <td>
                <a href="<?php echo base_url("hardware/exthd");?>"><img src="<?php echo base_url("/assets/layout/upgrade.jpg");?>" border="0"></a> External harddrive:
            </td>
            <td>
                0.1 Gb capacity
            </td>
        </tr>
        </tbody>
        </table>
    </div>
</div>