/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.1.41 : Database - slavehack
Copyright Advocaite 3013
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `files` */

DROP TABLE IF EXISTS `files`;

CREATE TABLE `files` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `vps_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `size` bigint(255) NOT NULL,
  `last_edit` bigint(255) NOT NULL,
  `hidden` int(11) NOT NULL,
  `hidden_lvl` varchar(255) NOT NULL,
  `ext` varchar(11) NOT NULL,
  `text` text NOT NULL,
  `hhd_type` int(2) NOT NULL DEFAULT '0',
  `is_folder` int(2) NOT NULL DEFAULT '0',
  `folder_id` int(11) NOT NULL DEFAULT '0',
  `fileid` int(11) NOT NULL DEFAULT '0',
  `master_id` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT charset=utf8;

/*Table structure for table `logfile` */

DROP TABLE IF EXISTS `logfile`;

CREATE TABLE `logfile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vps_ip` varchar(255) NOT NULL DEFAULT '1.1.1.1',
  `text` text NOT NULL,
  `last_edit` int(11) NOT NULL,
  `vps_id` int(11) NOT NULL,
  `temp_text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT charset=utf8;

/*Table structure for table `process` */

DROP TABLE IF EXISTS `process`;

CREATE TABLE `process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_started` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `ram_usage` int(11) NOT NULL,
  `vps_id` int(11) NOT NULL,
  `time_finished` int(11) NOT NULL,
  `target_ip` varchar(255) NOT NULL DEFAULT '1.1.1.1',
  `file_id` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(32) NOT NULL DEFAULT 'unnamed',
  `file_text` varchar(255) NOT NULL,
  `folder_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=250 DEFAULT charset=utf8;

/*Table structure for table `slaves` */

DROP TABLE IF EXISTS `slaves`;

CREATE TABLE `slaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vps_ip` varchar(255) NOT NULL DEFAULT '1.1.1.1',
  `master_ip` varchar(255) NOT NULL DEFAULT '1.1.1.1',
  `password` varchar(255) NOT NULL DEFAULT 'REW##@R',
  `task_type` int(11) NOT NULL DEFAULT '0',
  `time_start_task` int(11) NOT NULL DEFAULT '0',
  `vps_id` int(11) NOT NULL DEFAULT '0',
  `master_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT charset=utf8;

/*Table structure for table `tips` */

DROP TABLE IF EXISTS `tips`;

CREATE TABLE `tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tip` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT charset=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_joined` varchar(255) NOT NULL DEFAULT 'UNIX_TIMESTAMP',
  `vpspass` varchar(255) NOT NULL,
  `vpsip` varchar(255) NOT NULL,
  `hardrive_size` varchar(255) NOT NULL DEFAULT '32212254720',
  `cpu_speed` varchar(255) NOT NULL DEFAULT '1',
  `conection_speed` varchar(255) NOT NULL DEFAULT '1',
  `external_hd_size` varchar(255) NOT NULL DEFAULT '3221225472',
  `server_text` text NOT NULL,
  `rank` varchar(255) NOT NULL DEFAULT 'nooblet',
  `reputation` bigint(255) NOT NULL DEFAULT '0',
  `spam_sent` bigint(255) NOT NULL DEFAULT '0',
  `ip_resets` int(11) NOT NULL DEFAULT '2',
  `warez_sold` bigint(255) NOT NULL DEFAULT '0',
  `torrents_tracked` bigint(255) NOT NULL DEFAULT '0',
  `npc_type` int(11) NOT NULL DEFAULT '0',
  `last_update` int(11) NOT NULL,
  `time_spent` int(11) NOT NULL,
  `homepage` varchar(255) NOT NULL DEFAULT '1.1.1.1',
  `connected_to` varchar(255) NOT NULL DEFAULT '0',
  `time_spent_crack` bigint(255) NOT NULL DEFAULT '0',
  `last_ip_reset` int(11) NOT NULL DEFAULT '0',
  `last_bot_check` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT charset=utf8;

/*Table structure for table `webservice` */

DROP TABLE IF EXISTS `webservice`;

CREATE TABLE `webservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vpsid` int(11) NOT NULL,
  `text` text NOT NULL,
  `vpsip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT charset=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
