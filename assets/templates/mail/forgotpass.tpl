Hello {USERNAME},

Below is your new password to access your account on Vendetta Wars. 

Username: {USERNAME}
Email:    {EMAIL}
Password: {PASSWORD}

When you first login, you will be asked to change your password.

Thanks
Vendetta Wars Admin