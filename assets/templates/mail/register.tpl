Hello {USERNAME},
  
  Thank-you for registering an account on Vendetta Wars.
  
  Before you can begin to play, you must first activate your account. To do that, follow the link below.
  
  http://{HOSTNAME}/activate.php?action=activate&userid={USERID}&actkey={ACTKEY}
  
  Thanks for joining.
Vendetta Wars Admins
