Hello {USERNAME},

Because you have changed the email address you use to login, you will need to re-activate your account. To do this, follow the link below.

http://{HOSTNAME}/activate.php?action=activate&userid={USERID}&actkey={ACTKEY}

Thanks
Vendetta Wars Admin