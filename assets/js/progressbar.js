/**
 * ProgressBar for jQuery
 *
 * @version 1 (29. Dec 2012)
 * @author Ivan Lazarevic
 * @requires jQuery
 * @see http://workshop.rs
 *
 * @param  {Number} percent
 * @param  {Number} $element progressBar DOM element
 */   var pressed = false; 
       var finished = false; 

       var date = new Date();
       var milisecStarted=date.getTime()-0;
       var milisecEnd=date.getTime()+(date.getTime()+100)*1000;
       var milisecTotalTime=milisecEnd-milisecStarted;

       function prog()
       {
            if (pressed == false) 
            { 
            pressed = true; 
            progBar(); 
            }
       } 
       function progBar()
       {
            if (finished == false )
            {
                var now = new Date();
                milisecNow=now.getTime();
        
                var miliSecondsLeft=Math.ceil((milisecEnd-milisecNow)/100);
                if(miliSecondsLeft<0){miliSecondsLeft=0;}
                                                                     
                var hours=Math.floor(miliSecondsLeft/36000);
                var minutes=Math.floor(miliSecondsLeft%36000/600);
                var seconds=Math.floor(miliSecondsLeft%36000%600)/10;
                if((seconds%1)==0)
                {
                    seconds=seconds+".0";
                }
                var output;
                if(hours>0)
                {
                    var output=hours+" hours, "+minutes+" minutes and "+seconds+" seconds.";
                }
                else
                {
                    if(minutes>0)
                    {
                        var output=minutes+" minute(s) and "+seconds+" seconds.";
                    }
                    else
                    {
                        var output=seconds+" seconds.";
                    }
                }        
        
                miliSecondsLeft=Math.floor(miliSecondsLeft)/10;        
                var procent=Math.floor(((milisecTotalTime-miliSecondsLeft*1000)/milisecTotalTime)*100);
                if(procent<=0)
                { 
                    procent=1; 
                }
                if (milisecEnd>=milisecNow)
                {
                    document.getElementById("d3").innerHTML= output;
                    document.getElementById("d1").innerHTML=procent+"%";            
            
                } 
                else 
                {                                                                           
                    finished = true;            
            
                    document.getElementById("d1").innerHTML="100% ready";            
                    document.getElementById("d3").innerHTML="<input class=form type=submit value=Continue></form>";        
                }        
                document.getElementById("d2").style.width=(procent/100)*300+"px";
                             
                setTimeout("progBar();", 100);    
            }
        }
        window.setTimeout("prog()",151);
function progressBar(percent, $element) {
   
	var progressBarWidth = percent * $element.width() / 100;
	$element.find('div').animate({ width: progressBarWidth }, 100).html(percent + "%&nbsp;");
}