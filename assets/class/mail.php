<?php
// +---------------------------------------+
// | Paypal Class                          |
// + --------------------------------------+
// | Used to send out emails from the cold |
// | wars, inserts correct headers etc     |
// +---------------------------------------+
// | Created by dj                         |
// | Copyright The Cold Wars 2007          |
// +---------------------------------------+

class mail_class {
	var $mail_from = 'support@vendettawars.com';
	var $mail_to;
	var $subject;
	
	function set_mail_vars($from,$to,$subject){
		//this->mail_from = $from;
		$this->mail_to = $to;
		$this->mail_subject = $subject;
	}
	
	function load_template($template){
		if( empty($template) ){
			die('No template has been set');
		}
		
		$tplfile = $_SERVER['DOCUMENT_ROOT'].'/templates/mail/'.$template.'.tpl';
		
		if( !@file_exists($tplfile) ){
			die('Template file does not exist');
		}
		
		if( !($fhandle = @fopen($tplfile,'r')) ){
			die('Template could not be opened');
		}
		
		$this->msg = fread($fhandle, filesize($tplfile));
		fclose($fhandle);
		
		return true;
	}
	
	function assign_vars($vars){
		$this->vars = $vars;
	}
	
	function send(){
		// Escape all quotes, else the eval will fail.
		$this->msg = str_replace ("'", "\'", $this->msg);
		$this->msg = preg_replace('#\{([a-z0-9\-_]*?)\}#is', "' . $\\1 . '", $this->msg);

		// Set vars
		reset ($this->vars);
		while (list($key, $val) = each($this->vars)) 
		{
			$$key = $val;
		}

		eval("\$this->msg = '$this->msg';");
		
		$headers .= 'From: Vendetta Wars <'.$this->mail_from.'>' . "\n";
		$headers .= 'Reply-To: Vendetta Wars <'.$this->mail_from.'>' . "\n";
		$headers .= 'Return-Path: Vendetta Wars <'.$this->mail_from.'>' . "\n";
		$headers .= "Message-ID: <" . $now . " ".$this->mail_from.">" . "\n";
		$headers .= "X-Mailer: PHP v" . phpversion() . "\n";
		
		$result = @mail($this->mail_to,$this->mail_subject,$this->msg,$headers);
		
		if( !result ){
			die('Unable to send mail');
		}
		
		return true;
	}
}
?>